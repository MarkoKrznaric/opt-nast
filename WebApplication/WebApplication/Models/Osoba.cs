﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class Osoba : IdentityUser
    {
        public Osoba()
        {
            PredAkGodSemOsoba = new HashSet<PredAkGodSemOsoba>();
            PredAkGodSemGrupa = new HashSet<PredAkGodSemGrupa>();
        }

        //public int Id { get; set; }
        [Required(ErrorMessage = "Ime ne smije biti prazno")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž]*", ErrorMessage = "Ime se mora sastojati samo od slova")]
        [DisplayName("Ime")]
        public string Ime { get; set; }
        [Required(ErrorMessage = "Prezime ne smije biti prazno")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž]*", ErrorMessage = "Prezime se mora sastojati samo od slova")]
        [DisplayName("Prezime")]
        public string Prezime { get; set; }
        [Required(ErrorMessage = "Mail ne smije biti prazan")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [DisplayName("Mail")]
        public string Mail { get; set; }
        [DisplayName("Uloga")]
        public int IdUloga { get; set; }
        [DisplayName("Zavod")]
        public int IdZavod { get; set; }
        [DisplayName("Radno mjesto")]
        public int IdRadnoMjesto { get; set; }
        [DisplayName("Organizacijska jedinica")]
        public Zavod IdZavodNavigation { get; set; }
        [DisplayName("Uloga")]
        public Uloga IdUlogaNavigation { get; set; }
        [DisplayName("Radno mjesto")]
        public RadnoMjesto IdRadnoMjestoNavigation { get; set; }
        [DisplayName("PredAkGodSemOsoba")]
        public ICollection<PredAkGodSemOsoba> PredAkGodSemOsoba { get; set; }
        [DisplayName("PredAkGodSemGrupa")]
        public ICollection<PredAkGodSemGrupa> PredAkGodSemGrupa { get; set; }
    }
}
