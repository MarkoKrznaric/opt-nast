﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class VrstaNastave
    {
        public VrstaNastave()
        {
            OptPred = new HashSet<OptPred>();
            PredAkGodSemGrupa = new HashSet<PredAkGodSemGrupa>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv ne smije biti prazan")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž|' ']*", ErrorMessage = "Naziv se mora sastojati samo od slova")]
        [DisplayName("Naziv")]
        public string Naziv { get; set; }
        [DisplayName("Tip nastave")]
        public int IdTipNastave { get; set; }
        [DisplayName("Tip nastave")]
        public TipNastave IdTipNastaveNavigation { get; set; }
        [DisplayName("Opterećenje predmeta")]
        public ICollection<OptPred> OptPred { get; set; }
        [DisplayName("PredAkGodSemGrupa")]
        public ICollection<PredAkGodSemGrupa> PredAkGodSemGrupa { get; set; }
    }
}
