﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class AkademskaGodina
    {
        public AkademskaGodina()
        {
            PredAkGodSem = new HashSet<PredAkGodSem>();
        }

        public int Id { get; set; }
        [DisplayName("Akademska godina")]
        public int Godina { get; set; }
        [DisplayName("PredAkGodSem")]
        public ICollection<PredAkGodSem> PredAkGodSem { get; set; }
    }
}
