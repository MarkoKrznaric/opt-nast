﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class EvidNastAktivnosti
    {
        public EvidNastAktivnosti()
        {
        }

        public int Id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Datum")]
        public DateTime? Datum { get; set; }
        [DisplayName("Školskih sati")]
        public float SkoSati { get; set; }
        [DisplayName("PredAkGodSemOsoba")]
        public int IdPredAkGodSemOsoba { get; set; }
        [DisplayName("Opterećenje predmeta")]
        public int IdOptPred { get; set; }
        [DisplayName("PredAkGodSemOsoba")]
        public PredAkGodSemOsoba IdPredAkGodSemOsobaNavigation { get; set; }
        [DisplayName("Opterećenje predmeta")]
        public OptPred IdOptPredNavigation { get; set; }
    }
}
