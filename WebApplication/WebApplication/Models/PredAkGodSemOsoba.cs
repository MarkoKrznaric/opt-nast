﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class PredAkGodSemOsoba
    {
        public PredAkGodSemOsoba()
        {
            EvidNastAktivnosti = new HashSet<EvidNastAktivnosti>();
        }

        public int Id { get; set; }
        [DisplayName("Osoba")]
        public string IdOsoba { get; set; }
        [DisplayName("UlogaNaPred")]
        public int IdUlogaNaPred { get; set; }
        [DisplayName("PredAkGodSem")]
        public int IdPredAkGodSem { get; set; }
        [DisplayName("Osoba")]
        public Osoba IdOsobaNavigation { get; set; }
        [DisplayName("UlogaNaPred")]
        public UlogaNaPred IdUlogaNaPredNavigation { get; set; }
        [DisplayName("PredAkGodSem")]
        public PredAkGodSem IdPredAkGodSemNavigation { get; set; }
        [DisplayName("EvidNastAktivnosti")]
        public ICollection<EvidNastAktivnosti> EvidNastAktivnosti { get; set; }
    }
}
