﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class RadnoMjesto
    {
        public RadnoMjesto()
        {
            Osoba = new HashSet<Osoba>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv ne smije biti prazan")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž|' ']*", ErrorMessage = "Naziv se mora sastojati samo od slova")]
        [DisplayName("Naziv")]
        public string Naziv { get; set; }
        [DisplayName("Norma sati")]
        public int NormaSati { get; set; }
        [DisplayName("Osoba")]
        public ICollection<Osoba> Osoba { get; set; }
    }
}
