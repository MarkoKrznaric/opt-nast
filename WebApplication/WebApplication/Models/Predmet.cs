﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class Predmet
    {
        public Predmet()
        {
            OptPred = new HashSet<OptPred>();
            PredAkGodSem = new HashSet<PredAkGodSem>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Šifra ne smije biti prazna")]
        [RegularExpression("^[0-9]{1,30}$", ErrorMessage = "Maksimalan broj znakova je 30")]
        [DisplayName("Šifra")]
        public int Sifra { get; set; }
        [Required(ErrorMessage = "Naziv ne smije biti prazan")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž|' ']*", ErrorMessage = "Naziv se mora sastojati samo od slova")]
        [DisplayName("Naziv")]
        public string Naziv { get; set; }
        [DisplayName("Zavod")]
        public int IdZavod { get; set; }
        [DisplayName("Razina studija")]
        public int IdRazinaStudij { get; set; }
        [DisplayName("Zavod")]
        public Zavod IdZavodNavigation { get; set; }
        [DisplayName("Razina studija")]
        public RazinaStudij IdRazinaStudijNavigation { get; set; }
        [DisplayName("Opterećenje predmeta")]
        public ICollection<OptPred> OptPred { get; set; }
        [DisplayName("PredAkGodSem")]
        public ICollection<PredAkGodSem> PredAkGodSem { get; set; }
    }
}
