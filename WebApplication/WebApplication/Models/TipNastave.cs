﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class TipNastave
    {
        public TipNastave()
        {
            VrstaNastave = new HashSet<VrstaNastave>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv ne smije biti prazan")]
        [StringLength(30, ErrorMessage = "Maksimalan broj znakova je 30")]
        [RegularExpression(@"[A-Ž|a-ž|' ']*", ErrorMessage = "Naziv se mora sastojati samo od slova")]
        [DisplayName("Naziv")]
        public string Naziv { get; set; }
        [DisplayName("Vrsta nastave")]
        public ICollection<VrstaNastave> VrstaNastave { get; set; }
    }
}
