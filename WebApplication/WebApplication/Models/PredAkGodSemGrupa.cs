﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class PredAkGodSemGrupa
    {
        public PredAkGodSemGrupa()
        {
        }

        public int Id { get; set; }
        [DisplayName("Grupa")]
        public int IdGrupa { get; set; }
        [DisplayName("Vrsta nastave")]
        public int IdVrNastave { get; set; }
        [DisplayName("Izvođač")]
        public string IdOsoba { get; set; }
        [DisplayName("PredAkGodSem")]
        public int IdPredAkGodSem { get; set; }
        [DisplayName("Grupa")]
        public Grupa IdGrupaNavigation { get; set; }
        [DisplayName("Vrsta nastave")]
        public VrstaNastave IdVrNastaveNavigation { get; set; }
        [DisplayName("Izvođač")]
        public Osoba IdOsobaNavigation { get; set; }
        [DisplayName("PredAkGodSem")]
        public PredAkGodSem IdPredAkGodSemNavigation { get; set; }
    }
}
