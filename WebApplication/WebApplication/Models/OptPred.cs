﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class OptPred
    {
        public OptPred()
        {
            EvidNastAktivnosti = new HashSet<EvidNastAktivnosti>();
        }

        public int Id { get; set; }
        [DisplayName("Sati ukupno")]
        public float SatiUk { get; set; }
        [DisplayName("Predmet")]
        public int IdPredmet { get; set; }
        [DisplayName("Vrsta nastave")]
        public int IdVrstaNastave { get; set; }
        [DisplayName("Tip nastave")]
        public Predmet IdPredmetNavigation { get; set; }
        [DisplayName("Vrsta nastave")]
        public VrstaNastave IdVrstaNastaveNavigation { get; set; }
        [DisplayName("EvidNastAktivnosti")]
        public ICollection<EvidNastAktivnosti> EvidNastAktivnosti { get; set; }
    }
}
