﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public partial class PredAkGodSem
    {
        public PredAkGodSem()
        {
            PredAkGodSemOsoba = new HashSet<PredAkGodSemOsoba>();
            PredAkGodSemGrupa = new HashSet<PredAkGodSemGrupa>();
        }

        public int Id { get; set; }
        [DisplayName("BrStud")]
        public int BrStud { get; set; }
        [DisplayName("AkGodina")]
        public int IdAkGodina { get; set; }
        [DisplayName("Semestar")]
        public int IdSemestar { get; set; }
        [DisplayName("Predmet")]
        public int IdPredmet { get; set; }
        [DisplayName("AkGodina")]
        public AkademskaGodina IdAkGodinaNavigation { get; set; }
        [DisplayName("Semestar")]
        public Semestar IdSemestarNavigation { get; set; }
        [DisplayName("Predmet")]
        public Predmet IdPredmetNavigation { get; set; }
        [DisplayName("PredAkGodSemGrupa")]
        public ICollection<PredAkGodSemGrupa> PredAkGodSemGrupa { get; set; }
        [DisplayName("PredAkGodSemOsoba")]
        public ICollection<PredAkGodSemOsoba> PredAkGodSemOsoba { get; set; }
    }
}
