﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredAkGodSemGrupaJSONController : Controller
    {

        private readonly Context _context;

        public PredAkGodSemGrupaJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class PredAkGodSemGrupaBody
        {
            public int IdGrupa { get; set; }
            public int IdVrNastave { get; set; }
            public string IdOsoba { get; set; }
            public int IdPredmet { get; set; }
            public int IdAkGodina { get; set; }
            public int IdSemestar { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSemGrupa> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedPredAkGodSemGrupa
        {
            public int Id { get; set; }
            public string Grupa { get; set; }
            public string VrNastave { get; set; }
            public string Osoba { get; set; }
            public string Predmet { get; set; }
            public string AkGodina { get; set; }
            public string Semestar { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class PredAkGodSemGrupaListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSemGrupa> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }

        /*tu sam stao*/
        [HttpGet]
        public async Task<JsonResult> GetList()
        {

            List<PredAkGodSemGrupa> zaIzbacit = new List<PredAkGodSemGrupa>();
            List<PredAkGodSemGrupa> list = await _context.PredAkGodSemGrupe.Include(m => m.IdGrupaNavigation).Include(m => m.IdOsobaNavigation).Include(m => m.IdVrNastaveNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();
            foreach(var x in list)
            {
                if (x.IdOsobaNavigation == null)
                    zaIzbacit.Add(x);
            }
            foreach(var z in zaIzbacit)
            {
                list.Remove(z);
            }
            /*List<SelectedPredAkGodSemOsoba> selectedList = (from predAkGodSemOsoba in list
                                                            select new SelectedPredAkGodSemOsoba
                                                            {
                                                                Id = predAkGodSemOsoba.Id,
                                                                Osoba = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : predAkGodSemOsoba.IdOsobaNavigation.Ime,
                                                                UlogaNaPred = (predAkGodSemOsoba.IdUlogaNaPredNavigation == null) ? "" : predAkGodSemOsoba.IdUlogaNaPredNavigation.Naziv,
                                                                AkGodina = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdAkGodinaNavigation.Godina.ToString(),
                                                                Predmet = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmetNavigation.Naziv,
                                                                Semestar = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdSemestarNavigation.Naziv,
                                                            }).ToList();*/

            List<SelectedPredAkGodSemGrupa> selectedList = (from predAkGodSemGrupa in list
                                                            select new SelectedPredAkGodSemGrupa
                                                            {
                                                                Id = predAkGodSemGrupa.Id,
                                                                Grupa = (predAkGodSemGrupa.IdGrupaNavigation == null) ? "" : predAkGodSemGrupa.IdGrupaNavigation.Naziv,
                                                                VrNastave = (predAkGodSemGrupa.IdVrNastaveNavigation == null) ? "" : predAkGodSemGrupa.IdVrNastaveNavigation.Naziv,
                                                                Osoba = (predAkGodSemGrupa.IdOsobaNavigation == null) ? "" : (predAkGodSemGrupa.IdOsobaNavigation.Ime + " " + predAkGodSemGrupa.IdOsobaNavigation.Prezime),
                                                                AkGodina = (predAkGodSemGrupa.IdPredAkGodSemNavigation == null) ? "" : _context.AkademskeGodine.FirstOrDefault(m => m.Id == predAkGodSemGrupa.IdPredAkGodSemNavigation.IdAkGodina).Godina.ToString(),
                                                                Predmet = (predAkGodSemGrupa.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == predAkGodSemGrupa.IdPredAkGodSemNavigation.IdPredmet).Naziv,
                                                                Semestar = (predAkGodSemGrupa.IdPredAkGodSemNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == predAkGodSemGrupa.IdPredAkGodSemNavigation.IdSemestar).Naziv,
                                                            }).ToList();

            /*foreach(var x in selectedList)
            {
                if (x.Osoba.Equals(""))
                    selectedList.Remove(x);
            }*/

            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predAkGodSemGrupa = await _context.PredAkGodSemGrupe.SingleOrDefaultAsync(m => m.Id == id);



            if (predAkGodSemGrupa == null)
            {
                return Json(resultObject);
            }

            _context.PredAkGodSemGrupe.Remove(predAkGodSemGrupa);

            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] PredAkGodSemGrupaBody predAkGodSemGrupaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Naletio na Postt" },
                Data = new ResultObject.DataObject { }
            };
            if (predAkGodSemGrupaBody == null)
            {
                return Json(resultObject);
            }
            //var idZaProvjerit = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar).Id;
            //if (_context.PredAkGodSemOsobe.Any(m => m.IdOsoba.ToString() == predAkGodSemOsobaBody.IdOsoba && m.IdUlogaNaPred == predAkGodSemOsobaBody.IdUlogaNaPred && m.IdPredAkGodSem == idZaProvjerit))
            //{
            //    resultObject.Meta.Error = 400;
            //    resultObject.Meta.Message = "Već postoji";
            //    return Json(resultObject);
            //}


            if (!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar))
            {
                resultObject.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(resultObject);
            }

            if (!_context.PredAkGodSemGrupe.Any(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem == _context.PredAkGodSemestri.FirstOrDefault(x => x.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && x.IdPredmet == predAkGodSemGrupaBody.IdPredmet && x.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id))
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu grupu/vrstu nastave";
                return Json(resultObject);
            }

            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id;

            if (_context.PredAkGodSemGrupe.Any(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdOsoba != 1.ToString() && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem == idZaPredAkGodSem))
            {
                resultObject.Meta.Message = "Već postoji osoba koja predaje na odabranoj grupi";
                return Json(resultObject);
            }


            /*List<int> idsVrNast = new List<int>();
            List<OptPred> listaOptPred = _context.OptPredmeta.Where(m => m.IdPredmet == predAkGodSemGrupaBody.IdPredmet).ToList();

            foreach (var x in listaOptPred)
            {
                idsVrNast.Add(x.IdVrstaNastave);
            }

            if(!idsVrNast.Contains(predAkGodSemGrupaBody.IdVrNastave))
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu vrstu nastave";
                return Json(resultObject);
            }


            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id;

            List<PredAkGodSemGrupa> zaProvjerit = _context.PredAkGodSemGrupe.Where(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem==idZaPredAkGodSem).ToList();

            if (zaProvjerit==null)
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu grupu";
                return Json(resultObject);
            }*/

            var predAkGodSemGrupa = await _context.PredAkGodSemGrupe.FirstOrDefaultAsync(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem == idZaPredAkGodSem);

            predAkGodSemGrupa.IdOsoba = predAkGodSemGrupaBody.IdOsoba;
            predAkGodSemGrupa.IdOsobaNavigation = _context.Osobe.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdOsoba);

            /*var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id;


            var predAkGodSemGrupaModel = new PredAkGodSemGrupa
            {

                IdOsoba = predAkGodSemGrupaBody.IdOsoba,
                IdOsobaNavigation = _context.Osobe.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdOsoba),

                IdGrupa = predAkGodSemGrupaBody.IdGrupa,
                IdGrupaNavigation = _context.Grupe.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdGrupa),

                IdVrNastave = predAkGodSemGrupaBody.IdVrNastave,
                IdVrNastaveNavigation = _context.VrsteNastave.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdVrNastave),

                IdPredAkGodSem = idZaPredAkGodSem,
                IdPredAkGodSemNavigation = _context.PredAkGodSemestri.FirstOrDefault(m => m.Id == idZaPredAkGodSem)
            };*/


            try
            {
                TryValidateModel(predAkGodSemGrupa);

                _context.Entry(predAkGodSemGrupa).State = EntityState.Modified;
                //_context.PredAkGodSemGrupe.Add(predAkGodSemGrupaModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }




            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] PredAkGodSemGrupaBody predAkGodSemGrupaBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predAkGodSemGrupa = await _context.PredAkGodSemGrupe.FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);
            if (!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar))
            {
                resultObject.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(resultObject);
            }

            if (!_context.PredAkGodSemGrupe.Any(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem == _context.PredAkGodSemestri.FirstOrDefault(x => x.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && x.IdPredmet == predAkGodSemGrupaBody.IdPredmet && x.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id))
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu grupu/vrstu nastave";
                return Json(resultObject);
            }

            /*List<int> idsVrNast = new List<int>();
            List<OptPred> listaOptPred = _context.OptPredmeta.Where(m => m.IdPredmet == predAkGodSemGrupaBody.IdPredmet).ToList();

            foreach (var x in listaOptPred)
            {
                idsVrNast.Add(x.IdVrstaNastave);
            }

            if (!idsVrNast.Contains(predAkGodSemGrupaBody.IdVrNastave))
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu vrstu nastave";
                return Json(resultObject);
            }*/


            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id;

            /*List<PredAkGodSemGrupa> zaProvjerit = _context.PredAkGodSemGrupe.Where(m => m.IdGrupa == predAkGodSemGrupaBody.IdGrupa && m.IdVrNastave == predAkGodSemGrupaBody.IdVrNastave && m.IdPredAkGodSem == idZaPredAkGodSem).ToList();

            if (zaProvjerit == null)
            {
                resultObject.Meta.Message = "Predmet ne sadrži odabranu grupu";
                return Json(resultObject);
            }*/


            //var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemGrupaBody.IdAkGodina && m.IdPredmet == predAkGodSemGrupaBody.IdPredmet && m.IdSemestar == predAkGodSemGrupaBody.IdSemestar).Id;



            if (predAkGodSemGrupa == null)
            {
                return Json(resultObject);
            }

            predAkGodSemGrupa.IdOsoba = predAkGodSemGrupaBody.IdOsoba;
            predAkGodSemGrupa.IdOsobaNavigation = _context.Osobe.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdOsoba);

            predAkGodSemGrupa.IdGrupa = predAkGodSemGrupaBody.IdGrupa;
            predAkGodSemGrupa.IdGrupaNavigation = _context.Grupe.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdGrupa);

            predAkGodSemGrupa.IdVrNastave = predAkGodSemGrupaBody.IdVrNastave;
            predAkGodSemGrupa.IdVrNastaveNavigation = _context.VrsteNastave.FirstOrDefault(m => m.Id == predAkGodSemGrupaBody.IdVrNastave);

            predAkGodSemGrupa.IdPredAkGodSem = idZaPredAkGodSem;
            predAkGodSemGrupa.IdPredAkGodSemNavigation = _context.PredAkGodSemestri.FirstOrDefault(m => m.Id == idZaPredAkGodSem);

            try
            {
                TryValidateModel(predAkGodSemGrupa);
                //TryValidateModel(p);

                _context.Entry(predAkGodSemGrupa).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }


            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}

