﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class OpterecenjePredmetaJSONController : Controller
    {

        private readonly Context _context;

        public OpterecenjePredmetaJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class OpterecenjePredmetaBody
        {
            public float SatiUk { get; set; }
            public int IdVrstaNastave { get; set; }
            public int OdabirPredmeta { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedOpterecenjePredmeta> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedOpterecenjePredmeta
        {
            public int Id { get; set; }
            public float SatiUk { get; set; }
            public string Predmet { get; set; }
            public string VrstaNastave { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class OpterecenjePredmetaListObject
        {
            public class DataObject
            {
                public List<SelectedOpterecenjePredmeta> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }

        /*tu sam stao*/
        [HttpGet]
        public async Task<JsonResult> GetList(int odabirPredmeta)
        {

            List<OptPred> list = await _context.OptPredmeta.Include(m => m.IdPredmetNavigation).Include(m => m.IdVrstaNastaveNavigation).Where(m => m.IdPredmet == odabirPredmeta).ToListAsync();

            List<SelectedOpterecenjePredmeta> selectedList = (from optPred in list
                                                       select new SelectedOpterecenjePredmeta
                                                       {
                                                           Id = optPred.Id,
                                                           SatiUk = optPred.SatiUk,
                                                           VrstaNastave = (optPred.IdVrstaNastaveNavigation == null) ? "" : optPred.IdVrstaNastaveNavigation.Naziv,
                                                       }).ToList();



            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var opterecenjePredmeta = await _context.OptPredmeta.SingleOrDefaultAsync(m => m.Id == id);



            if (opterecenjePredmeta == null)
            {
                return Json(resultObject);
            }

            _context.OptPredmeta.Remove(opterecenjePredmeta);

            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] OpterecenjePredmetaBody opterecenjePredmetaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Naletio na Post" },
                Data = new ResultObject.DataObject { }
            };
            if (opterecenjePredmetaBody == null)
            {
                return Json(resultObject);
            }

            if (_context.OptPredmeta.Any(m => m.IdPredmet == opterecenjePredmetaBody.OdabirPredmeta && m.IdVrstaNastave == opterecenjePredmetaBody.IdVrstaNastave))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }



            var opterecenjePredmetaModel = new OptPred
            {
                SatiUk = opterecenjePredmetaBody.SatiUk,

                IdPredmet = opterecenjePredmetaBody.OdabirPredmeta,
                IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.OdabirPredmeta),

                IdVrstaNastave = opterecenjePredmetaBody.IdVrstaNastave,
                IdVrstaNastaveNavigation = _context.VrsteNastave.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.IdVrstaNastave),
            };


            try
            {
                TryValidateModel(opterecenjePredmetaModel);

                _context.OptPredmeta.Add(opterecenjePredmetaModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }



            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] OpterecenjePredmetaBody opterecenjePredmetaBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var optPred = await _context.OptPredmeta.FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);



            if (optPred == null)
            {
                return Json(resultObject);
            }

            optPred.SatiUk = opterecenjePredmetaBody.SatiUk;
            optPred.IdPredmet = opterecenjePredmetaBody.OdabirPredmeta;
            optPred.IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.OdabirPredmeta);

            optPred.IdVrstaNastave = opterecenjePredmetaBody.IdVrstaNastave;
            optPred.IdVrstaNastaveNavigation = _context.VrsteNastave.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.IdVrstaNastave);

            try
            {
                TryValidateModel(optPred);
                //TryValidateModel(p);

                _context.Entry(optPred).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }


            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}