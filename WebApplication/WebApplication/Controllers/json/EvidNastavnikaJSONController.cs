﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class EvidNastavnikaJSONController : Controller
    {

        private readonly Context _context;

        public EvidNastavnikaJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public double UkupnoSatiPred { get; set; }
                public double UkupnoSatiVjez { get; set; }
                public double UkupnoSatiSem { get; set; }
                public double UkupnoSati { get; set; }
                public double UkupnoSatiPredRasp { get; set; }
                public double UkupnoSatiVjezRasp { get; set; }
                public double UkupnoSatiSemRasp { get; set; }
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public string Naziv { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string Predmet { get; set; }
            public string Studij { get; set; }
            public string Semestar { get; set; }
            public string SatiPred { get; set; }
            public string SatiVjez { get; set; }
            public string SatiSem { get; set; }
            public string RaspPred { get; set; }
            public string RaspVjez { get; set; }
            public string RaspSem { get; set; }
            //public string UkSati { get; set; }
            //public string Razlika { get; set; }
        }


        [HttpGet]
        public JsonResult GetList(int odabirAkGod, string ime, string prezime)
        {
            ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = odabirAkGod.ToString() + ime + prezime },
                Data = new ResultObject.DataObject { }
            };
            //if (odabirOsobe == null)
            //    provjera.Meta.Message = "Bok";
            //if (odabirOsobe == null || odabirAkGod == 0)
            //    return Json(provjera);

            List<SelectedZavod> selectedList = new List<SelectedZavod>();
            var ukupnoSati = 0.0;
            var ukupnoSatiPred = 0.0;
            var ukupnoSatiVjez = 0.0;
            var ukupnoSatiSem = 0.0;
            var ukupnoSatiPredRasp = 0.0;
            var ukupnoSatiVjezRasp = 0.0;
            var ukupnoSatiSemRasp = 0.0;


            string odabirOsobe = _context.Osobe.FirstOrDefault(m => m.Ime.Equals(ime) && m.Prezime.Equals(prezime)).Id;

                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdAkGodina == odabirAkGod).ToList();
                foreach (var p in predAkGodSem)
                {
                    var predavanja = 0.0;
                    var vjezbe = 0.0;
                    var seminari = 0.0;
                    var ukupno = 0.0;
                    int brojZapisa = 0;
                    var Rasppredavanja = 0.0;
                    var Raspvjezbe = 0.0;
                    var Raspseminari = 0.0;

                List<PredAkGodSemOsoba> predAkGodSemOsoba = _context.PredAkGodSemOsobe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var o in predAkGodSemOsoba)
                    {
                    
                    brojZapisa++;

                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var n in predAkGodSemGrupa)
                    {
                        brojZapisa++;
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == p.IdPredmet && m.IdVrstaNastave == n.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == n.IdVrNastave).IdTipNastave).Naziv;
                        if (tipNast.Equals("Predavanja"))
                        {
                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                if (o.IdOsoba != 1.ToString())
                                    Rasppredavanja = Rasppredavanja + optPredSatiUk * 3;
                            }
                            else
                            {
                                if (o.IdOsoba != 1.ToString())
                                    Rasppredavanja = Rasppredavanja + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                Raspvjezbe = Raspvjezbe + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                Raspseminari = Raspseminari + optPredSatiUk * 1.5;
                        }
                        ukupnoSatiPredRasp = ukupnoSatiPredRasp + Rasppredavanja;
                        ukupnoSatiVjezRasp = ukupnoSatiVjezRasp + Raspvjezbe;
                        ukupnoSatiSemRasp = ukupnoSatiSemRasp + Raspseminari;
                    }

                    List<EvidNastAktivnosti> evid = _context.EvidencijeNastAktivnosti.Where(m => m.IdPredAkGodSemOsoba == o.Id).ToList();
                        foreach (var e in evid)
                        {

                            var idVrsteNast = _context.OptPredmeta.FirstOrDefault(m => m.Id == e.IdOptPred).IdVrstaNastave;
                            var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == idVrsteNast).IdTipNastave).Naziv;
                            if (tipNast.Equals("Predavanja"))
                            {
                                predavanja = predavanja + e.SkoSati;
                                if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                                {
                                    ukupno = ukupno + e.SkoSati * 3;
                                }
                                else
                                {
                                    ukupno = ukupno + e.SkoSati * 2;
                                }
                            }
                            if (tipNast.Equals("Vježbe"))
                            {
                                vjezbe = vjezbe + e.SkoSati;
                                ukupno = ukupno + e.SkoSati;
                            }
                            if (tipNast.Equals("Seminar"))
                            {
                                seminari = seminari + e.SkoSati;
                                ukupno = ukupno + e.SkoSati * 1.5;
                            }
                        }
                    }
                    if (brojZapisa == 0)
                        continue;
                    if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                    {
                        predavanja = predavanja * 3;
                    }
                    else
                    {
                        predavanja = predavanja * 2;
                    }
                    seminari = seminari * 1.5;
                    ukupnoSati = ukupnoSati + ukupno;
                    ukupnoSatiPred = ukupnoSatiPred + predavanja;
                    ukupnoSatiVjez = ukupnoSatiVjez + vjezbe;
                    ukupnoSatiSem = ukupnoSatiSem + seminari;



                var nazivPred = _context.Predmeti.FirstOrDefault(m => m.Id == p.IdPredmet).Naziv;

                var idrazinestudija = _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij;
                var nazivStudija = _context.RazineStudija.FirstOrDefault(m => m.Id == idrazinestudija).Naziv;
                //provjera.Meta.Message
                //return Json(provjera);

                SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = 1,
                        Predmet = nazivPred,
                        Studij = nazivStudija,
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        SatiPred = predavanja.ToString(),
                        SatiVjez = vjezbe.ToString(),
                        SatiSem = seminari.ToString(),
                        RaspPred = Rasppredavanja.ToString(),
                        RaspVjez = Raspvjezbe.ToString(),
                        RaspSem = Raspseminari.ToString()
                    };
                    selectedList.Add(dodaj);
                }




            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject
                {
                    List = selectedList,
                    UkupnoSati = ukupnoSati,
                    UkupnoSatiPred = ukupnoSatiPred,
                    UkupnoSatiVjez = ukupnoSatiVjez,
                    UkupnoSatiSem = ukupnoSatiSem,
                    UkupnoSatiPredRasp = ukupnoSatiPredRasp,
                    UkupnoSatiVjezRasp = ukupnoSatiVjezRasp,
                    UkupnoSatiSemRasp = ukupnoSatiSemRasp
                }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.SingleOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            _context.Zavodi.Remove(zavod);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody zavodBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (zavodBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Zavodi.Any(m => m.Naziv == zavodBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var zavodModel = new Zavod
            {
                Naziv = zavodBody.Naziv
            };

            try
            {
                TryValidateModel(zavodModel);

                _context.Zavodi.Add(zavodModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}