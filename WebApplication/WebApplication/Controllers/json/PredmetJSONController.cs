﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredmetJSONController : Controller
    {

        private readonly Context _context;

        public PredmetJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class PredmetBody
        {
            public int Sifra { get; set; }
            public string Naziv { get; set; }
            public int IdZavod { get; set; }
            public int IdRazinaStudij { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedPredmet> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedPredmet
        {
            public int Id { get; set; }
            public int Sifra { get; set; }
            public string Naziv { get; set; }
            public string Zavod { get; set; }
            public string RazinaStudij { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class PredmetListObject
        {
            public class DataObject
            {
                public List<SelectedPredmet> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }


        [HttpGet]
        public async Task<JsonResult> GetList()
        {

            List<Predmet> list = await _context.Predmeti.Include(m => m.IdZavodNavigation).
                Include(m => m.IdRazinaStudijNavigation).ToListAsync();

            List<SelectedPredmet> selectedList = (from predmet in list
                                                select new SelectedPredmet
                                                {
                                                     Id = predmet.Id,
                                                     Sifra = predmet.Sifra,
                                                     Naziv = predmet.Naziv,
                                                     Zavod = (predmet.IdZavodNavigation == null) ? "" : predmet.IdZavodNavigation.Naziv,
                                                     RazinaStudij = (predmet.IdRazinaStudijNavigation == null) ? "" : predmet.IdRazinaStudijNavigation.Naziv,
                                                }).ToList();



            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predmet = await _context.Predmeti.SingleOrDefaultAsync(m => m.Id == id);



            if (predmet == null)
            {
                return Json(resultObject);
            }

            _context.Predmeti.Remove(predmet);
            
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] PredmetBody predmetBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Naletio na Post" },
                Data = new ResultObject.DataObject { }
            };
            if (predmetBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Predmeti.Any(m => m.Sifra == predmetBody.Sifra))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }



            var predmetModel = new Predmet
            {
                Sifra = predmetBody.Sifra,
                Naziv = predmetBody.Naziv,

                IdZavod = predmetBody.IdZavod,
                IdZavodNavigation = _context.Zavodi.FirstOrDefault(m => m.Id == predmetBody.IdZavod),

                IdRazinaStudij = predmetBody.IdRazinaStudij,
                IdRazinaStudijNavigation = _context.RazineStudija.FirstOrDefault(m => m.Id == predmetBody.IdRazinaStudij),
            };
            
            /*var akGodPredmet = await _context.AkademskeGodine.FirstOrDefaultAsync(m => m.Id == predmetBody.AkGod);
            var semPredmet = await _context.Semestri.FirstOrDefaultAsync(m => m.Id == predmetBody.Semestar);
            var predPredmet = await _context.Predmeti.FirstOrDefaultAsync(m => m.Id == predmetBody.Sifra);
            var PredAkGodSemModel = new PredAkGodSem
            {
                IdAkGodina = akGodPredmet.Id,
                IdAkGodinaNavigation = akGodPredmet,

                IdSemestar = semPredmet.Id,
                IdSemestarNavigation = semPredmet,

                IdPredmet = predPredmet.Id,
                IdPredmetNavigation = predPredmet,

                BrStud = 0
            };*/

            try
            {
                TryValidateModel(predmetModel);
            
                _context.Predmeti.Add(predmetModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }

            /*var PredAkGodSemModel = new PredAkGodSem
            {
                IdAkGodina = predmetBody.AkGod,
                IdAkGodinaNavigation = await _context.AkademskeGodine.FirstOrDefaultAsync(m => m.Id == predmetBody.AkGod),

                IdPredmet = predmetModel.Id,
                IdPredmetNavigation = await _context.Predmeti.FirstOrDefaultAsync(m => m.Sifra == predmetBody.Sifra),

                IdSemestar = predmetBody.Semestar,
                IdSemestarNavigation = await _context.Semestri.FirstOrDefaultAsync(m => m.Id == predmetBody.Semestar),

                BrStud = 0
            };
            try
            {
                TryValidateModel(PredAkGodSemModel);

                _context.PredAkGodSemestri.Add(PredAkGodSemModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException )
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }*/

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] PredmetBody predmetBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };
            
            var predmet = await _context.Predmeti.Include(m => m.PredAkGodSem).FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);

            

            if (predmet == null)
            {
                return Json(resultObject);
            }

            predmet.Sifra = predmetBody.Sifra;
            predmet.Naziv = predmetBody.Naziv;
            predmet.IdZavod = predmetBody.IdZavod;
            predmet.IdRazinaStudij = predmetBody.IdRazinaStudij;
            predmet.IdZavodNavigation = _context.Zavodi.FirstOrDefault(m => m.Id == predmetBody.IdZavod);
            predmet.IdRazinaStudijNavigation = _context.RazineStudija.FirstOrDefault(m => m.Id == predmetBody.IdRazinaStudij);

            try
            {
                TryValidateModel(predmet);
                //TryValidateModel(p);

                _context.Entry(predmet).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Grešk" };
                return Json(resultObject);
            }
            /*foreach (var item in predmet.PredAkGodSem)
            {
                if (!_context.Semestri.Any(x => x.Id == item.IdSemestar))
                {
                    _context.PredAkGodSemestri.Remove(item);
                }
                if (!_context.AkademskeGodine.Any(x => x.Id == item.IdAkGodina))
                {
                    _context.PredAkGodSemestri.Remove(item);
                }
            }
            */

            /*var predAkGodSem = await _context.PredAkGodSemestri.FirstOrDefaultAsync(m => m.IdPredmet == id && m.IdAkGodina==predmetBody.AkGod && m.IdSemestar == predmetBody.Semestar);

            predAkGodSem.IdPredmet = id;
            predAkGodSem.IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == id);

            predAkGodSem.IdSemestar = predmetBody.Semestar;
            predAkGodSem.IdSemestarNavigation = _context.Semestri.FirstOrDefault(m => m.Id == predmetBody.Semestar);

            predAkGodSem.IdAkGodina = predmetBody.AkGod;
            predAkGodSem.IdAkGodinaNavigation = _context.AkademskeGodine.FirstOrDefault(m => m.Id == predmetBody.AkGod);
            predAkGodSem.BrStud = 0;

            try
            {
                TryValidateModel(predAkGodSem);
                //TryValidateModel(p);

                _context.Entry(predAkGodSem).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Grešk" };
                return Json(resultObject);
            }*/
            /*if (predAkGodSem == null)
            {
                PredAkGodSem n = new PredAkGodSem
                {
                    IdPredmet = id,
                    IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == id),

                    IdSemestar = predmetBody.Semestar,
                    IdSemestarNavigation = _context.Semestri.FirstOrDefault(m => m.Id == predmetBody.Semestar),

                    IdAkGodina = predmetBody.AkGod,
                    IdAkGodinaNavigation = _context.AkademskeGodine.FirstOrDefault(m => m.Id == predmetBody.AkGod),
                    BrStud = 0,
                
                };
                try
                {
                    TryValidateModel(n);

                    _context.PredAkGodSemestri.Add(n);

                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateException )
                {
                    resultObject.Meta.Message = "Greška2";
                    return Json(resultObject);
                }
            }*/

            /*predAkGodSem.IdPredmet = id;
            predAkGodSem.IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == id);

            predAkGodSem.IdSemestar = predmetBody.Semestar;
            predAkGodSem.IdSemestarNavigation = _context.Semestri.FirstOrDefault(m => m.Id == predmetBody.Semestar);

            predAkGodSem.IdAkGodina = predmetBody.AkGod;
            predAkGodSem.IdAkGodinaNavigation = _context.AkademskeGodine.FirstOrDefault(m => m.Id == predmetBody.AkGod);
            predAkGodSem.BrStud = 0;*/
            //var predAkGodSem = await _context.PredAkGodSemestri.FirstOrDefaultAsync(m => m.IdPredmet == id && m.IdAkGodina==predmetBody.AkGod && m.IdSemestar == predmetBody.Semestar);

            /*PredAkGodSem p=null;
            p = await _context.PredAkGodSemestri.FirstOrDefaultAsync(m => m.IdPredmet == id && m.IdAkGodina==predmetBody.AkGod && m.IdSemestar == predmetBody.Semestar);

            if (!_context.PredAkGodSemestri.Any(m => m.IdPredmet == id && m.IdAkGodina == predmetBody.AkGod && m.IdSemestar == predmetBody.Semestar)) {
                PredAkGodSem predAkGodSem = new PredAkGodSem
                {
                    IdPredmet = id,
                    IdPredmetNavigation = await _context.Predmeti.FirstOrDefaultAsync(m => m.Id == id),


                    IdSemestar = predmetBody.Semestar,
                    IdSemestarNavigation = await _context.Semestri.FirstOrDefaultAsync(m => m.Id == predmetBody.Semestar),

                    IdAkGodina = predmetBody.AkGod,
                    IdAkGodinaNavigation = await _context.AkademskeGodine.FirstOrDefaultAsync(m => m.Id == predmetBody.AkGod),

                    BrStud = 5
                };
                p = predAkGodSem;
            }*/




            /*try
            {
                //TryValidateModel(p);
                TryValidateModel(predAkGodSem);

                _context.Entry(predAkGodSem).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Grešk" };
                return Json(resultObject);
            }*/

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}