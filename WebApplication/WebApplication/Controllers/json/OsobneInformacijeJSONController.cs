﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class OsobneInformacijeJSONController : Controller
    {

        private readonly Context _context;

        public OsobneInformacijeJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public string Naziv { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Mail { get; set; }
            public string Zavod { get; set; }
            public string RadnoMjesto { get; set; }
            public string NormaSati { get; set; }
        }


        [HttpGet("{id}")]
        public JsonResult GetList([FromRoute] string id)
        {

            /*ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = id },
                Data = new ResultObject.DataObject { }
            };
            return Json(provjera);*/
            //List<PredAkGodSemOsoba> list =  _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();

            List<PredAkGodSemOsoba> list = _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).Where(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(id)).Id)).ToList();

            /*List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Naziv = zavod.Naziv
                                                }).ToList();*/

            List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Ime = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : predAkGodSemOsoba.IdOsobaNavigation.Ime,
                                                    Prezime = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : predAkGodSemOsoba.IdOsobaNavigation.Prezime,
                                                    Mail = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : predAkGodSemOsoba.IdOsobaNavigation.Mail,
                                                    Zavod = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : _context.Zavodi.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdOsobaNavigation.IdZavod).Naziv,
                                                    //Voditelj
                                                    RadnoMjesto = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : _context.RadnaMjesta.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdOsobaNavigation.IdZavod).Naziv,
                                                    NormaSati = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : _context.RadnaMjesta.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdOsobaNavigation.IdZavod).NormaSati.ToString(),
                                                }).ToList();

            SelectedZavod osobneInf = new SelectedZavod();

            foreach (var s in selectedList)
            {
                osobneInf = s;
            }

            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(osobneInf);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.SingleOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            _context.Zavodi.Remove(zavod);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody zavodBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (zavodBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Zavodi.Any(m => m.Naziv == zavodBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var zavodModel = new Zavod
            {
                Naziv = zavodBody.Naziv
            };

            try
            {
                TryValidateModel(zavodModel);

                _context.Zavodi.Add(zavodModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}