﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredAkGodSemJSONController : Controller
    {

        private readonly Context _context;

        public PredAkGodSemJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class PredAkGodSemBody
        {
            public int BrStud { get; set; }
            public int IdPredmet { get; set; }
            public int IdAkGodina { get; set; }
            public int IdSemestar { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSem> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedPredAkGodSem
        {
            public int Id { get; set; }
            public int BrStud { get; set; }
            public string Predmet { get; set; }
            public string AkGodina { get; set; }
            public string Semestar { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class PredAkGodSemListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSem> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }

        /*tu sam stao*/
        [HttpGet]
        public async Task<JsonResult> GetList()
        {

            List<PredAkGodSem> list = await _context.PredAkGodSemestri.Include(m => m.IdSemestarNavigation).Include(m => m.IdPredmetNavigation).Include(m => m.IdAkGodinaNavigation).ToListAsync();

            List<SelectedPredAkGodSem> selectedList = (from predAkGodSem in list
                                                              select new SelectedPredAkGodSem
                                                              {
                                                                  Id = predAkGodSem.Id,
                                                                  BrStud = predAkGodSem.BrStud,
                                                                  AkGodina = (predAkGodSem.IdAkGodinaNavigation == null) ? "" : predAkGodSem.IdAkGodinaNavigation.Godina.ToString(),
                                                                  Predmet = (predAkGodSem.IdPredmetNavigation == null) ? "" : predAkGodSem.IdPredmetNavigation.Naziv,
                                                                  Semestar = (predAkGodSem.IdSemestarNavigation == null) ? "" : predAkGodSem.IdSemestarNavigation.Naziv,
                                                              }).ToList();



            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predAkGodSem = await _context.PredAkGodSemestri.SingleOrDefaultAsync(m => m.Id == id);



            if (predAkGodSem == null)
            {
                return Json(resultObject);
            }

            _context.PredAkGodSemestri.Remove(predAkGodSem);

            List<PredAkGodSemGrupa> grupe = new List<PredAkGodSemGrupa>();

            grupe = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == id).ToList();

            if(grupe!=null)
            {
                foreach(var grupa in grupe)
                {
                    _context.PredAkGodSemGrupe.Remove(grupa);
                }
            }

            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] PredAkGodSemBody predAkGodSemBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = predAkGodSemBody.BrStud.ToString() + predAkGodSemBody.IdAkGodina.ToString() + "  " + predAkGodSemBody.IdPredmet.ToString() + "  " + predAkGodSemBody.IdSemestar.ToString() },
                Data = new ResultObject.DataObject { }
            };
            //return Json(resultObject);
            if (predAkGodSemBody == null)
            {
                return Json(resultObject);
            }

            if (_context.PredAkGodSemestri.Any(m => m.IdPredmet == predAkGodSemBody.IdPredmet && m.IdAkGodina == predAkGodSemBody.IdAkGodina && m.IdSemestar == predAkGodSemBody.IdSemestar))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }



            var predAkGodSemModel = new PredAkGodSem
            {
                BrStud = predAkGodSemBody.BrStud,

                IdPredmet = predAkGodSemBody.IdPredmet,
                IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == predAkGodSemBody.IdPredmet),

                IdAkGodina = predAkGodSemBody.IdAkGodina,
                IdAkGodinaNavigation = _context.AkademskeGodine.FirstOrDefault(m => m.Id == predAkGodSemBody.IdAkGodina),

                IdSemestar = predAkGodSemBody.IdSemestar,
                IdSemestarNavigation = _context.Semestri.FirstOrDefault(m => m.Id == predAkGodSemBody.IdSemestar),
            };


            try
            {
                TryValidateModel(predAkGodSemModel);

                _context.PredAkGodSemestri.Add(predAkGodSemModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }

            List<int> idsVrNast = new List<int>();
            List<OptPred> listaOptPred = _context.OptPredmeta.Where(m => m.IdPredmet == predAkGodSemBody.IdPredmet).ToList();

            foreach(var x in listaOptPred)
            {
                idsVrNast.Add(x.IdVrstaNastave);
            }

            List<PredAkGodSemGrupa> grupe = new List<PredAkGodSemGrupa>();

            foreach(var ids in idsVrNast)
            {
                if (_context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == ids).IdTipNastave).Id == 1)
                {

                    int brGrupa = (predAkGodSemBody.BrStud / 80);

                    if (predAkGodSemBody.BrStud % 80 > 0)
                        brGrupa++;

                    for (int i=0; i<brGrupa; i++)
                    {
                        var predAkGodSemGrupaModel = new PredAkGodSemGrupa
                        {

                            IdVrNastave = ids,
                            IdVrNastaveNavigation = await _context.VrsteNastave.FirstOrDefaultAsync(m => m.Id == ids),

                            IdPredAkGodSem = predAkGodSemModel.Id,
                            IdPredAkGodSemNavigation = predAkGodSemModel,

                            IdOsoba = "1",
                            IdOsobaNavigation = null,

                            IdGrupa = i + 1,
                            IdGrupaNavigation = await _context.Grupe.FirstOrDefaultAsync(m => m.Id == (i + 1)),
                        };
                        //grupe.Add(predAkGodSemGrupaModel);
                        try
                        {
                            TryValidateModel(predAkGodSemGrupaModel);

                            _context.PredAkGodSemGrupe.Add(predAkGodSemGrupaModel);

                            await _context.SaveChangesAsync();

                        }
                        catch (DbUpdateException /* ex */)
                        {
                            resultObject.Meta.Message = "Greška2";
                            return Json(resultObject);
                        }
                    }
                }
                if (_context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == ids).IdTipNastave).Id == 2)
                {
                    int brGrupa = predAkGodSemBody.BrStud / 10;

                    if (predAkGodSemBody.BrStud % 10 > 0)
                        brGrupa++;

                    for (int i = 0; i < brGrupa; i++)
                    {
                        var predAkGodSemGrupaModel = new PredAkGodSemGrupa
                        {

                            IdVrNastave = ids,
                            IdVrNastaveNavigation = await _context.VrsteNastave.FirstOrDefaultAsync(m => m.Id == ids),

                            IdPredAkGodSem = predAkGodSemModel.Id,
                            IdPredAkGodSemNavigation = predAkGodSemModel,

                            IdOsoba = "1",
                            IdOsobaNavigation = null,

                            IdGrupa = i + 1,
                            IdGrupaNavigation = await _context.Grupe.FirstOrDefaultAsync(m => m.Id == (i + 1)),
                        };
                        //grupe.Add(predAkGodSemGrupaModel);
                        try
                        {
                            TryValidateModel(predAkGodSemGrupaModel);

                            _context.PredAkGodSemGrupe.Add(predAkGodSemGrupaModel);

                            await _context.SaveChangesAsync();

                        }
                        catch (DbUpdateException /* ex */)
                        {
                            resultObject.Meta.Message = "Greška2";
                            return Json(resultObject);
                        }
                    }
                }
                if (_context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == ids).IdTipNastave).Id == 3)
                {
                    int brGrupa = predAkGodSemBody.BrStud / 30;

                    if (predAkGodSemBody.BrStud % 30 > 0)
                        brGrupa++;

                    for (int i = 0; i < brGrupa; i++)
                    {
                        var predAkGodSemGrupaModel = new PredAkGodSemGrupa
                        {

                            IdVrNastave = ids,
                            IdVrNastaveNavigation = await _context.VrsteNastave.FirstOrDefaultAsync(m => m.Id == ids),

                            IdPredAkGodSem = predAkGodSemModel.Id,
                            IdPredAkGodSemNavigation = predAkGodSemModel,

                            IdOsoba = "1",
                            IdOsobaNavigation = null,

                            IdGrupa = i + 1,
                            IdGrupaNavigation = await _context.Grupe.FirstOrDefaultAsync(m => m.Id == (i + 1)),
                        };
                        //grupe.Add(predAkGodSemGrupaModel);
                        try
                        {
                            TryValidateModel(predAkGodSemGrupaModel);

                            _context.PredAkGodSemGrupe.Add(predAkGodSemGrupaModel);

                            await _context.SaveChangesAsync();

                        }
                        catch (DbUpdateException /* ex */)
                        {
                            resultObject.Meta.Message = "Greška2";
                            return Json(resultObject);
                        }
                    }
                }
            }

            /*foreach(var grupa in grupe)
            {
                try
                {
                    TryValidateModel(grupa);

                    _context.PredAkGodSemGrupe.Add(grupa);

                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateException )
                {
                    resultObject.Meta.Message = "Greška2";
                    return Json(resultObject);
                }
            }*/


            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] PredAkGodSemBody predAkGodSemBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno(Put)" },
                Data = new ResultObject.DataObject { }
            };
            /*
            var optPred = await _context.OptPredmeta.FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);



            if (optPred == null)
            {
                return Json(resultObject);
            }

            optPred.SatiUk = opterecenjePredmetaBody.SatiUk;
            optPred.IdPredmet = opterecenjePredmetaBody.IdPredmet;
            optPred.IdPredmetNavigation = _context.Predmeti.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.IdPredmet);

            optPred.IdVrstaNastave = opterecenjePredmetaBody.IdVrstaNastave;
            optPred.IdVrstaNastaveNavigation = _context.VrsteNastave.FirstOrDefault(m => m.Id == opterecenjePredmetaBody.IdVrstaNastave);

            try
            {
                TryValidateModel(optPred);
                //TryValidateModel(p);

                _context.Entry(optPred).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }


            resultObject.Meta = new MetaObject { };*/
            return Json(resultObject);
        }
    }
}