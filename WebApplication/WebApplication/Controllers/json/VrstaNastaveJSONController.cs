﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class VrstaNastaveJSONController : Controller
    {

        private readonly Context _context;

        public VrstaNastaveJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class VrstaNastaveBody
        {
            public string Naziv { get; set; }
            public int IdTipNastave { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedVrstaNastave> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedVrstaNastave
        {
            public int Id { get; set; }
            public string Naziv { get; set; }
            public string TipNastave { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class VrstaNastaveListObject
        {
            public class DataObject
            {
                public List<SelectedVrstaNastave> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }


        [HttpGet]
        public async Task<JsonResult> GetList()
        {

            List<VrstaNastave> list = await _context.VrsteNastave.Include(m => m.IdTipNastaveNavigation).ToListAsync();

            List<SelectedVrstaNastave> selectedList = (from vrstaNastave in list
                                                  select new SelectedVrstaNastave
                                                  {
                                                      Id = vrstaNastave.Id,
                                                      Naziv = vrstaNastave.Naziv,
                                                      TipNastave = (vrstaNastave.IdTipNastaveNavigation == null) ? "" : vrstaNastave.IdTipNastaveNavigation.Naziv,
                                                  }).ToList();



            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var vrstaNastave = await _context.VrsteNastave.SingleOrDefaultAsync(m => m.Id == id);



            if (vrstaNastave == null)
            {
                return Json(resultObject);
            }

            _context.VrsteNastave.Remove(vrstaNastave);

            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] VrstaNastaveBody vrstaNastaveBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Naletio na Post" },
                Data = new ResultObject.DataObject { }
            };
            if (vrstaNastaveBody == null)
            {
                return Json(resultObject);
            }

            if (_context.VrsteNastave.Any(m => m.Naziv == vrstaNastaveBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }



            var vrstaNastaveModel = new VrstaNastave
            {
                Naziv = vrstaNastaveBody.Naziv,

                IdTipNastave = vrstaNastaveBody.IdTipNastave,
                IdTipNastaveNavigation = _context.TipoviNastave.FirstOrDefault(m => m.Id == vrstaNastaveBody.IdTipNastave),
            };

            
            try
            {
                TryValidateModel(vrstaNastaveModel);

                _context.VrsteNastave.Add(vrstaNastaveModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }

            

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] VrstaNastaveBody vrstaNastaveBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var vrstaNastave = await _context.VrsteNastave.FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);



            if (vrstaNastave == null)
            {
                return Json(resultObject);
            }

            vrstaNastave.Naziv = vrstaNastaveBody.Naziv;
            vrstaNastave.IdTipNastave = vrstaNastaveBody.IdTipNastave;
            vrstaNastave.IdTipNastaveNavigation = _context.TipoviNastave.FirstOrDefault(m => m.Id == vrstaNastaveBody.IdTipNastave);

            try
            {
                TryValidateModel(vrstaNastave);
                //TryValidateModel(p);

                _context.Entry(vrstaNastave).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }
            

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}