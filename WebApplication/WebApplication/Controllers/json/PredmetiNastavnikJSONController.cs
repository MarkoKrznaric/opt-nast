﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredmetiNastavnikJSONController : Controller
    {

        private readonly Context _context;

        public PredmetiNastavnikJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public string Naziv { get; set; }
        }

        public class SelectedEvidZaPopis
        {
            public int Id { get; set; }
            public string SkoSati { get; set; }
            public string Datum { get; set; }
            public string VrNast { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string Sifra { get; set; }
            public string Predmet { get; set; }
            public string Semestar { get; set; }
            public string AkGodina { get; set; }
            public string Uloga { get; set; }
            public string RazinaStudij { get; set; }
            public List<SelectedEvidZaPopis> EvidList { get; set; }
        }


        [HttpGet("{id}")]
        public JsonResult GetList([FromRoute] string id)
        {

            /*ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = id },
                Data = new ResultObject.DataObject { }
            };
            return Json(provjera);*/
            //List<PredAkGodSemOsoba> list =  _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();

            List<PredAkGodSemOsoba> list = _context.PredAkGodSemOsobe.Include(m=> m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).Where(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(id)).Id)).ToList();

            /*List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Naziv = zavod.Naziv
                                                }).ToList();*/

            List<SelectedZavod> selectedList = new List<SelectedZavod>();

            foreach(var item in list)
            {
                List<SelectedEvidZaPopis> evidList = new List<SelectedEvidZaPopis>();
                var evidZaPopis = _context.EvidencijeNastAktivnosti.Include(a => a.IdOptPredNavigation).Include(a => a.IdPredAkGodSemOsobaNavigation).Where(a => a.IdPredAkGodSemOsoba.Equals(item.Id)).ToList();

                foreach(var x in evidZaPopis)
                {
                    SelectedEvidZaPopis evid = new SelectedEvidZaPopis
                    {
                        Id = x.Id,
                        SkoSati = x.SkoSati.ToString(),
                        Datum = x.Datum.ToString(),
                        VrNast = _context.VrsteNastave.FirstOrDefault(m => m.Id == x.IdOptPredNavigation.IdVrstaNastave).Naziv
                    };
                    evidList.Add(evid);
                }

                SelectedZavod selectedZavod = new SelectedZavod
                {
                    Id = (item.IdPredAkGodSemNavigation == null) ? 0 : _context.Predmeti.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdPredmet).Id,
                    Sifra = (item.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdPredmet).Sifra.ToString(),
                    Predmet = (item.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdPredmet).Naziv,
                    Uloga = (item.IdUlogaNaPredNavigation == null) ? "" : item.IdUlogaNaPredNavigation.Naziv,
                    AkGodina = (item.IdPredAkGodSemNavigation == null) ? "" : _context.AkademskeGodine.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdAkGodina).Godina.ToString(),
                    Semestar = (item.IdPredAkGodSemNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdSemestar).Naziv,
                    RazinaStudij = (item.IdPredAkGodSemNavigation == null) ? "" : _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == item.IdPredAkGodSemNavigation.IdPredmet).IdRazinaStudij).Naziv,
                    EvidList = evidList
                };
                selectedList.Add(selectedZavod);
            }

            /*List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Sifra = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmet).Sifra.ToString(),
                                                    Predmet = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmet).Naziv,
                                                    Uloga = (predAkGodSemOsoba.IdUlogaNaPredNavigation == null) ? "" : predAkGodSemOsoba.IdUlogaNaPredNavigation.Naziv,
                                                    AkGodina = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.AkademskeGodine.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdAkGodina).Godina.ToString(),
                                                    Semestar = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdSemestar).Naziv,
                                                    RazinaStudij = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmet).IdRazinaStudij).Naziv,
                                                }).ToList();*/


            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.SingleOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            _context.Zavodi.Remove(zavod);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody zavodBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (zavodBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Zavodi.Any(m => m.Naziv == zavodBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var zavodModel = new Zavod
            {
                Naziv = zavodBody.Naziv
            };

            try
            {
                TryValidateModel(zavodModel);

                _context.Zavodi.Add(zavodModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}