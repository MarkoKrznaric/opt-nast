﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class SelectController : Controller
    {

        private readonly Context _context;

        public SelectController(Context context)
        {
            _context = context;
        }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ListNameObject
        {
            public class DataObject
            {
                public List<SelectedBody> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class SelectedBody
        {
            public int Value { get; set; }
            public string Label { get; set; }
        }

        

        public class OsobaBody
        {
            public string Value { get; set; }
            public string Label { get; set; }
        }

        public class SelectObject
        {
            public class DataObject
            {
                public List<OsobaBody> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        [HttpGet]
        public async Task<JsonResult> Osobe()
        {
            List<Osoba> listObjects = await _context.Osobe.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju osoba" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<OsobaBody> selectedList = (from item in listObjects
                                            select new OsobaBody
                                            {
                                                Value = item.Id,
                                                Label = item.Ime + " " + item.Prezime
                                            }).ToList();

            SelectObject resultObject = new SelectObject
            {
                Meta = new MetaObject { },
                Data = new SelectObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> RadnaMjesta()
        {
            List<RadnoMjesto> listObjects = await _context.RadnaMjesta.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju radnih mjesta" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> UlogeNaPred()
        {
            List<UlogaNaPred> listObjects = await _context.UlogeNaPred.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju uloga" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }
        [HttpGet]
        public async Task<JsonResult> Grupe()
        {
            List<Grupa> listObjects = await _context.Grupe.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju grupe" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> TipoviNastave()
        {
            List<TipNastave> listObjects = await _context.TipoviNastave.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju tipa nastave" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet("{id}")]
        public JsonResult PredmetiNastavnika([FromRoute] string id)
        {
            List<PredAkGodSemOsoba> list = _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).Where(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(id)).Id)).ToList();

            if (list == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju predmeta" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in list
                                               select new SelectedBody
                                               {
                                                   Value = _context.Predmeti.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdPredmet).Id,
                                                   Label = _context.Predmeti.FirstOrDefault(m => m.Id == item.IdPredAkGodSemNavigation.IdPredmet).Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> VrsteNastave()
        {
            List<VrstaNastave> listObjects = await _context.VrsteNastave.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju vrste nastave" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> Predmeti()
        {
            List<Predmet> listObjects = await _context.Predmeti.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju predmeta" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> Zavodi()
        {
            List<Zavod> listObjects = await _context.Zavodi.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju zavoda" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public JsonResult GetMojZavod(string idUsername)
        {
            /*List<Zavod> listObjects = await _context.Zavodi.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju zavoda" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };*/
            var Osoba = _context.Osobe.FirstOrDefault(x => x.UserName.Equals(idUsername));
            var zavod = _context.Zavodi.FirstOrDefault(m => m.Id == Osoba.IdZavod).Naziv;
            return Json(zavod);
        }

        [HttpGet]
        public async Task<JsonResult> RazineStudija()
        {
            List<RazinaStudij> listObjects = await _context.RazineStudija.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju studija" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpGet]
        public async Task<JsonResult> AkGod()
        {
            List<AkademskaGodina> listObjects = await _context.AkademskeGodine.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju AkGod" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Godina.ToString()
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }
        [HttpGet]
        public async Task<JsonResult> Semestar()
        {
            List<Semestar> listObjects = await _context.Semestri.ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Greška u dohvaćanju semestra" },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectedBody> selectedList = (from item in listObjects
                                               select new SelectedBody
                                               {
                                                   Value = item.Id,
                                                   Label = item.Naziv
                                               }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }
    }
}