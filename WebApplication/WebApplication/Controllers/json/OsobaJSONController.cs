﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class OsobaJsonController : Controller
    {

        private readonly Context _context;
        private readonly UserManager<Osoba> _userManager;
        private readonly SignInManager<Osoba> _signInManager;

        public OsobaJsonController(Context context, UserManager<Osoba> userManager, SignInManager<Osoba> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedBody> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class OsobaBody
        {
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Mail { get; set; }
            public string UserName { get; set; }
            public string Uloga { get; set; }
        }

        public class SelectedBody
        {
            public string Id { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Mail { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Uloga { get; set; }
            public string RadnoMjesto { get; set; }
            public string Zavod { get; set; }
        }

        public class UserBody
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Uloga { get; set; }
            public string Mail { get; set; }
            public int IdRadnoMjesto { get; set; }
            public int IdZavod { get; set; }
        }

        public class RegisterReturnObject
        {
            public class DataObject
            {
                public string Role { get; set; }
            }
            public class MetaObject
            {
                public int Error { get; set; }
                public string Message { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        private string GetRole(Osoba user)
        {
            var claims = _userManager.GetClaimsAsync(user).Result;
            var role = "";
            if (claims.Count > 0)
            {
                role = claims.FirstOrDefault().Value.ToString();
            }
            return role;
        }

        public class ListNameObject
        {
            public class DataObject
            {
                public List<SelectBody> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class SelectBody
        {
            public string Value { get; set; }
            public string Label { get; set; }
        }

        [HttpGet]
        public async Task<JsonResult> PopisivacSelect()
        {
            List<Osoba> listObjects = await _context.Osobe.Where(x => GetRole(x).Equals("Popisivač")).ToListAsync();

            if (listObjects == null)
            {
                ResultObject errorObject = new ResultObject
                {
                    Meta = new MetaObject { Error = 400, Message = "Failed to obtain persons." },
                    Data = new ResultObject.DataObject { }
                };

                return Json(errorObject);
            }

            List<SelectBody> selectedList = (from item in listObjects
                                             select new SelectBody
                                             {
                                                 Value = item.Id,
                                                 Label = item.UserName
                                             }).ToList();

            ListNameObject resultObject = new ListNameObject
            {
                Meta = new MetaObject { },
                Data = new ListNameObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        
        [HttpGet]
        public async Task<JsonResult> GetList()
        {
            List<Osoba> list = await _context.Osobe.Include(m => m.IdRadnoMjestoNavigation).
                Include(m => m.IdZavodNavigation).ToListAsync();


            List<SelectedBody> selectedList = (from item in list
                                               select new SelectedBody
                                               {
                                                   Id = item.Id,
                                                   Uloga = (GetRole(item) == "") ? "Nema uloge" : GetRole(item),
                                                   Username = item.UserName,
                                                   Password = item.PasswordHash,
                                                   Ime = item.Ime,
                                                   Prezime = item.Prezime,
                                                   Mail = item.Mail,
                                                   RadnoMjesto = (item.IdRadnoMjestoNavigation == null) ? "" : item.IdRadnoMjestoNavigation.Naziv,
                                                   Zavod = (item.IdZavodNavigation == null) ? "" : item.IdZavodNavigation.Naziv
                                               }).ToList();

            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }
        [HttpPost]
        public async Task<JsonResult> Post([FromBody] UserBody userBody)
        {
            RegisterReturnObject resultObject = new RegisterReturnObject
            {
                Meta = new RegisterReturnObject.MetaObject { Error = 404, Message = "Nije pronađeno tijelo" },
                Data = new RegisterReturnObject.DataObject { }
            };
            if (userBody == null)
            {

                return Json(resultObject);
            }
            if (_context.Osobe.Any(x => x.UserName.Equals(userBody.Username)))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Username zauzet";
                return Json(resultObject);
            }
            var user = new Osoba
            {
                UserName = userBody.Username,
                Ime = userBody.Ime,
                Prezime = userBody.Prezime,
                Mail = userBody.Mail,
                IdRadnoMjesto = userBody.IdRadnoMjesto,
                IdRadnoMjestoNavigation = _context.RadnaMjesta.FirstOrDefault(m => m.Id == userBody.IdRadnoMjesto),

                IdZavod = userBody.IdZavod,
                IdZavodNavigation = _context.Zavodi.FirstOrDefault(m => m.Id == userBody.IdZavod),

            };
            var result = await _userManager.CreateAsync(user, userBody.Password);
            if (result.Errors.Any())
            {
                resultObject.Meta.Error = 500;
                resultObject.Meta.Message = "";
                foreach (IdentityError error in result.Errors)
                {
                    resultObject.Meta.Message += error.Description + " ";
                }
            }
            if (result.Succeeded)
            {
                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, userBody.Uloga));

                await _signInManager.SignInAsync(user, isPersistent: false);

                resultObject.Meta = new RegisterReturnObject.MetaObject { };
                return Json(resultObject);
            }

            //resultObject.Meta.Error = 400;
            //  resultObject.Meta.Message = "Greška";
            resultObject.Data = new RegisterReturnObject.DataObject { Role = userBody.Uloga };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] string id, [FromBody] UserBody userBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            //            var osoba = await _context.Osobe.FirstOrDefaultAsync(m => m.Id == id);
            var osoba = await _userManager.FindByIdAsync(id);
            if (osoba == null)
            {
                return Json(resultObject);
            }

            var currentRole = GetRole(osoba);
            if (!userBody.Uloga.Equals(currentRole))
            {
                var currentClaim = _userManager.GetClaimsAsync(osoba).Result.FirstOrDefault();
                await _userManager.ReplaceClaimAsync(osoba, currentClaim, new Claim(ClaimTypes.Role, userBody.Uloga));
            }
            if (userBody.Password != osoba.PasswordHash)
            {
                var resetToken = await _userManager.GeneratePasswordResetTokenAsync(osoba);
                await _userManager.ResetPasswordAsync(osoba, resetToken, userBody.Password);
            }

            osoba.Ime = userBody.Ime;
            osoba.Prezime = userBody.Prezime;
            osoba.UserName = userBody.Username;
            osoba.Mail = userBody.Mail;
            osoba.IdRadnoMjesto = userBody.IdRadnoMjesto;
            osoba.IdZavod = userBody.IdZavod;
            osoba.IdRadnoMjestoNavigation = _context.RadnaMjesta.FirstOrDefault(m => m.Id == userBody.IdRadnoMjesto);
            osoba.IdZavodNavigation = _context.Zavodi.FirstOrDefault(m => m.Id == userBody.IdZavod);

            try
            {
                TryValidateModel(osoba);

                _context.Entry(osoba).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] string id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var osoba = await _context.Osobe.SingleOrDefaultAsync(m => m.Id == id);

            if (osoba == null)
            {
                return Json(resultObject);
            }

            _context.Osobe.Remove(osoba);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

    }
}