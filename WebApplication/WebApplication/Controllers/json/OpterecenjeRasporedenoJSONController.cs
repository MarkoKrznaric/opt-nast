﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class OpterecenjeRasporedenoJSONController : Controller
    {

        private readonly Context _context;

        public OpterecenjeRasporedenoJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public double UkupnoSatiPred { get; set; }
                public double UkupnoSatiVjez { get; set; }
                public double UkupnoSatiSem { get; set; }
                public double UkupnoSati { get; set; }
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public string Naziv { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string Predmet { get; set; }
            public string Studij { get; set; }
            public string Semestar { get; set; }
            public string SatiPred { get; set; }
            public string SatiVjez { get; set; }
            public string SatiSem { get; set; }
            //public string UkSati { get; set; }
            //public string Razlika { get; set; }
        }


        [HttpGet]
        public JsonResult GetList(int odabirPredmeta, string odabirOsobe, int odabirAkGod, int odabirSem)
        {
            ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = odabirPredmeta.ToString() + odabirOsobe + odabirAkGod.ToString() + odabirSem.ToString() },
                Data = new ResultObject.DataObject { }
            };
            //if (odabirOsobe == null)
            //    provjera.Meta.Message = "Bok";
            if (odabirOsobe == null || odabirAkGod == 0)
                return Json(provjera);

            List<SelectedZavod> selectedList = new List<SelectedZavod>();
            var ukupnoSati = 0.0;
            var ukupnoSatiPred = 0.0;
            var ukupnoSatiVjez = 0.0;
            var ukupnoSatiSem = 0.0;

            if (odabirPredmeta == 0 && odabirSem == 0)
            {

                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdAkGodina == odabirAkGod).ToList();
                foreach (var p in predAkGodSem)
                {
                    var predavanja = 0.0;
                    var vjezbe = 0.0;
                    var seminari = 0.0;
                    int brojZapisa = 0;
                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var o in predAkGodSemGrupa)
                    {
                        brojZapisa++;
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == p.IdPredmet && m.IdVrstaNastave == o.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == o.IdVrNastave).IdTipNastave).Naziv;

                        if (tipNast.Equals("Predavanja"))
                        {

                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 3;
                            }
                            else
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                vjezbe = vjezbe + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                seminari = seminari + optPredSatiUk * 1.5;
                        }
                    }
                    if (brojZapisa == 0)
                        continue;


                    ukupnoSati = ukupnoSati + predavanja + vjezbe + seminari;
                    ukupnoSatiPred = ukupnoSatiPred + predavanja;
                    ukupnoSatiVjez = ukupnoSatiVjez + vjezbe;
                    ukupnoSatiSem = ukupnoSatiSem + seminari;

                    SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = 1,
                        Predmet = _context.Predmeti.FirstOrDefault(m => m.Id == p.IdPredmet).Naziv,
                        Studij = _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv,
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        SatiPred = predavanja.ToString(),
                        SatiVjez = vjezbe.ToString(),
                        SatiSem = seminari.ToString(),
                    };

                    selectedList.Add(dodaj);
                }
            }

            if (odabirPredmeta == 0 && odabirSem != 0)
            {

                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdAkGodina == odabirAkGod && m.IdSemestar == odabirSem).ToList();
                foreach (var p in predAkGodSem)
                {
                    var predavanja = 0.0;
                    var vjezbe = 0.0;
                    var seminari = 0.0;
                    var ukupno = 0.0;
                    int brojZapisa = 0;
                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var o in predAkGodSemGrupa)
                    {
                        brojZapisa++;
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == p.IdPredmet && m.IdVrstaNastave == o.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == o.IdVrNastave).IdTipNastave).Naziv;
                        if (tipNast.Equals("Predavanja"))
                        {
                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 3;
                            }
                            else
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                vjezbe = vjezbe + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                seminari = seminari + optPredSatiUk * 1.5;
                        }
                    }
                    if (brojZapisa == 0)
                        continue;
                    
                    ukupnoSati = ukupnoSati + predavanja + vjezbe + seminari;
                    ukupnoSatiPred = ukupnoSatiPred + predavanja;
                    ukupnoSatiVjez = ukupnoSatiVjez + vjezbe;
                    ukupnoSatiSem = ukupnoSatiSem + seminari;
                    
                    SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = 1,
                        Predmet = _context.Predmeti.FirstOrDefault(m => m.Id == p.IdPredmet).Naziv,
                        Studij = _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).Id).Naziv,
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        SatiPred = predavanja.ToString(),
                        SatiVjez = vjezbe.ToString(),
                        SatiSem = seminari.ToString(),
                    };
                    selectedList.Add(dodaj);
                }
            }

            if (odabirPredmeta != 0 && odabirSem == 0)
            {

                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdAkGodina == odabirAkGod && m.IdPredmet == odabirPredmeta).ToList();
                foreach (var p in predAkGodSem)
                {
                    var predavanja = 0.0;
                    var vjezbe = 0.0;
                    var seminari = 0.0;
                    var ukupno = 0.0;
                    int brojZapisa = 0;
                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var o in predAkGodSemGrupa)
                    {
                        brojZapisa++;
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == p.IdPredmet && m.IdVrstaNastave == o.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == o.IdVrNastave).IdTipNastave).Naziv;
                        if (tipNast.Equals("Predavanja"))
                        {
                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 3;
                            }
                            else
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                vjezbe = vjezbe + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                seminari = seminari + optPredSatiUk * 1.5;
                        }
                    }
                    if (brojZapisa == 0)
                        continue;
                    
                    ukupnoSati = ukupnoSati + predavanja + vjezbe + seminari;
                    ukupnoSatiPred = ukupnoSatiPred + predavanja;
                    ukupnoSatiVjez = ukupnoSatiVjez + vjezbe;
                    ukupnoSatiSem = ukupnoSatiSem + seminari;
                    SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = 1,
                        Predmet = _context.Predmeti.FirstOrDefault(m => m.Id == p.IdPredmet).Naziv,
                        Studij = _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).Id).Naziv,
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        SatiPred = predavanja.ToString(),
                        SatiVjez = vjezbe.ToString(),
                        SatiSem = seminari.ToString(),
                    };
                    selectedList.Add(dodaj);
                }
            }

            if (odabirPredmeta != 0 && odabirSem != 0)
            {

                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdAkGodina == odabirAkGod && m.IdPredmet == odabirPredmeta && m.IdSemestar == odabirSem).ToList();
                foreach (var p in predAkGodSem)
                {
                    var predavanja = 0.0;
                    var vjezbe = 0.0;
                    var seminari = 0.0;
                    var ukupno = 0.0;
                    int brojZapisa = 0;
                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id && m.IdOsoba.Equals(odabirOsobe)).ToList();
                    foreach (var o in predAkGodSemGrupa)
                    {
                        brojZapisa++;
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == p.IdPredmet && m.IdVrstaNastave == o.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == o.IdVrNastave).IdTipNastave).Naziv;
                        if (tipNast.Equals("Predavanja"))
                        {
                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 3;
                            }
                            else
                            {
                                if (o.IdOsoba != 1.ToString())
                                    predavanja = predavanja + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                vjezbe = vjezbe + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            if (o.IdOsoba != 1.ToString())
                                seminari = seminari + optPredSatiUk * 1.5;
                        }
                    }
                    if (brojZapisa == 0)
                        continue;

                    ukupnoSati = ukupnoSati + predavanja + vjezbe + seminari;
                    ukupnoSatiPred = ukupnoSatiPred + predavanja;
                    ukupnoSatiVjez = ukupnoSatiVjez + vjezbe;
                    ukupnoSatiSem = ukupnoSatiSem + seminari;
                    SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = 1,
                        Predmet = _context.Predmeti.FirstOrDefault(m => m.Id == p.IdPredmet).Naziv,
                        Studij = _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).Id).Naziv,
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        SatiPred = predavanja.ToString(),
                        SatiVjez = vjezbe.ToString(),
                        SatiSem = seminari.ToString(),
                    };
                    selectedList.Add(dodaj);
                }
            }

            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject
                {
                    List = selectedList,
                    UkupnoSati = ukupnoSati,
                    UkupnoSatiPred = ukupnoSatiPred,
                    UkupnoSatiVjez = ukupnoSatiVjez,
                    UkupnoSatiSem = ukupnoSatiSem
                }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.SingleOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            _context.Zavodi.Remove(zavod);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody zavodBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (zavodBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Zavodi.Any(m => m.Naziv == zavodBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var zavodModel = new Zavod
            {
                Naziv = zavodBody.Naziv
            };

            try
            {
                TryValidateModel(zavodModel);

                _context.Zavodi.Add(zavodModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}