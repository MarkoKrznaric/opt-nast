﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class EvidPredJSONController : Controller
    {

        private readonly Context _context;

        public EvidPredJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public int IdVrNast { get; set; }
            public string SkoSati { get; set; }
            public string Datum { get; set; }
            //public int IdPredmet { get; set; }
            public int OdabirPredmeta { get; set; }
            public string OdabirAkGod { get; set; }
            public string OdabirSem { get; set; }
            public string IdUsername { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string AkGodina { get; set; }
            public string Semestar { get; set; }
            public string VrNast { get; set; }
            public string SkoSati { get; set; }
            public string Datum { get; set; }
        }


        [HttpGet]
        public JsonResult GetList(int odabirPredmeta, string idUsername, string odabirAkGod, string odabirSem)
        {

            ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = odabirPredmeta.ToString() + idUsername + odabirAkGod + odabirSem },
                Data = new ResultObject.DataObject { }
            };
            //return Json(provjera);
            //List<PredAkGodSemOsoba> list =  _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();
            /*if (!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == odabirAkGod && m.IdPredmet == odabirPredmeta && m.IdSemestar == odabirSem))
            {
                provjera.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(provjera);
            }
            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == odabirAkGod && m.IdPredmet == odabirPredmeta && m.IdSemestar == odabirSem).Id;

            if (!_context.PredAkGodSemOsobe.Any(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(idUsername)).Id) && m.IdPredAkGodSem == idZaPredAkGodSem))
            {
                provjera.Meta.Message = "Osoba ne predaje na odabranom predmetu/akGod/semestru";
                return Json(provjera);
            }*/
            var idOdabirAkGod = _context.AkademskeGodine.FirstOrDefault(m => m.Godina.ToString().Equals(odabirAkGod)).Id;
            var idOdabirSem = _context.Semestri.FirstOrDefault(m => m.Naziv.Equals(odabirSem)).Id;

            //var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == idOdabirAkGod && m.IdPredmet == odabirPredmeta && m.IdSemestar == idOdabirSem).Id;

            var predAkGodSemPredmet = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdPredmet == odabirPredmeta && m.IdAkGodina == idOdabirAkGod && m.IdSemestar == idOdabirSem);
            /*List<int> idPred = new List<int>();
            foreach(var i in predAkGodSemPredmet)
            {
                idPred.Add(i.Id);
            }*/


            List<EvidNastAktivnosti> list = _context.EvidencijeNastAktivnosti.Include(m => m.IdPredAkGodSemOsobaNavigation).Include(m => m.IdOptPredNavigation)
                .Where(m => m.IdPredAkGodSemOsobaNavigation.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(idUsername)).Id) && m.IdPredAkGodSemOsobaNavigation.IdPredAkGodSem == predAkGodSemPredmet.Id).ToList();

            /*List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Naziv = zavod.Naziv
                                                }).ToList();*/

            List<SelectedZavod> selectedList = (from NastAkt in list
                                                select new SelectedZavod
                                                {
                                                    Id = NastAkt.Id,
                                                    //Grupa = (NastAkt.IdPredAkGodSemOsobaNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == _context.PredAkGodSemestri.FirstOrDefault(x => x.Id == NastAkt.IdPredAkGodSemOsobaNavigation.IdPredAkGodSem).IdSemestar).Naziv,
                                                    VrNast = (NastAkt.IdOptPredNavigation == null) ? "" : _context.VrsteNastave.FirstOrDefault(m => m.Id == NastAkt.IdOptPredNavigation.IdVrstaNastave).Naziv,
                                                    //TipNast = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmet).IdRazinaStudij).Naziv,
                                                    SkoSati = NastAkt.SkoSati.ToString(),
                                                    Datum = NastAkt.Datum.ToString(),
                                                }).ToList();


            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };
            return Json(resultObject);
        }



        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] int id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var nastAkt = await _context.EvidencijeNastAktivnosti.SingleOrDefaultAsync(m => m.Id == id);

            if (nastAkt == null)
            {
                return Json(resultObject);
            }

            _context.EvidencijeNastAktivnosti.Remove(nastAkt);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody nastAktBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = nastAktBody.IdUsername + nastAktBody.IdVrNast.ToString() + nastAktBody.SkoSati + nastAktBody.OdabirAkGod + nastAktBody.OdabirSem + nastAktBody.OdabirPredmeta.ToString() },
                Data = new ResultObject.DataObject { }
            };
            //return Json(resultObject);

            if (nastAktBody == null)
            {
                return Json(resultObject);
            }
            //resultObject.Meta.Message = nastAktBody.IdUsername + nastAktBody.IdAkGodina.ToString()
            //    +nastAktBody.IdSemestar.ToString() + nastAktBody.IdVrNast.ToString() + nastAktBody.OdabirPredmeta.ToString()
            //    +nastAktBody.SkoSati + nastAktBody.Datum;
            //return Json(resultObject);

            /*if (!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == nastAktBody.OdabirAkGod && m.IdPredmet == nastAktBody.OdabirPredmeta && m.IdSemestar == nastAktBody.OdabirSem))
            {
                resultObject.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(resultObject);
            }
            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == nastAktBody.OdabirAkGod && m.IdPredmet == nastAktBody.OdabirPredmeta && m.IdSemestar == nastAktBody.OdabirSem).Id;

            if (!_context.PredAkGodSemOsobe.Any(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(nastAktBody.IdUsername)).Id) && m.IdPredAkGodSem == idZaPredAkGodSem))
            {
                resultObject.Meta.Message = "Osoba ne predaje na odabranom predmetu/akGod/semestru";
                return Json(resultObject);
            }*/

            /*var idOdabirAkGod = _context.AkademskeGodine.FirstOrDefault(m => m.Godina.ToString().Equals(nastAktBody.OdabirAkGod)).Id;
            var idOdabirSem = _context.Semestri.FirstOrDefault(m => m.Naziv.Equals(nastAktBody.OdabirSem)).Id;

            var idPredAkGodSemestar = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == idOdabirAkGod && m.IdSemestar == idOdabirSem && m.IdPredmet == nastAktBody.OdabirPredmeta).Id;
            

            var idPredAkGodSemOs = _context.PredAkGodSemOsobe.FirstOrDefault(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(nastAktBody.IdUsername)).Id) && m.IdPredAkGodSem == idPredAkGodSemestar).Id;
            var idOpt = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == nastAktBody.OdabirPredmeta && m.IdVrstaNastave == nastAktBody.IdVrNast).Id;
            */
            var idPredAkGodSemestar = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == _context.AkademskeGodine.FirstOrDefault(x => x.Godina.ToString().Equals(nastAktBody.OdabirAkGod)).Id && m.IdSemestar == _context.Semestri.FirstOrDefault(z => z.Naziv.Equals(nastAktBody.OdabirSem)).Id && m.IdPredmet == nastAktBody.OdabirPredmeta).Id;
            var idPredAkGodSemOs = _context.PredAkGodSemOsobe.FirstOrDefault(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(nastAktBody.IdUsername)).Id) && m.IdPredAkGodSem == idPredAkGodSemestar).Id;
            var idOpt = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == nastAktBody.OdabirPredmeta && m.IdVrstaNastave == nastAktBody.IdVrNast).Id;

            var nastAktModel = new EvidNastAktivnosti
            {
                Datum = DateTime.Now,
                SkoSati = int.Parse(nastAktBody.SkoSati),

                IdPredAkGodSemOsoba = idPredAkGodSemOs,
                IdPredAkGodSemOsobaNavigation = _context.PredAkGodSemOsobe.FirstOrDefault(m => m.Id == idPredAkGodSemOs),

                IdOptPred = idOpt,
                IdOptPredNavigation = _context.OptPredmeta.FirstOrDefault(m => m.Id == idOpt)
            };
            //var idZaProvjerit = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar).Id;
            //if (_context.PredAkGodSemOsobe.Any(m => m.IdOsoba.ToString() == predAkGodSemOsobaBody.IdOsoba && m.IdUlogaNaPred == predAkGodSemOsobaBody.IdUlogaNaPred && m.IdPredAkGodSem == idZaProvjerit))
            //{
            //    resultObject.Meta.Error = 400;
            //    resultObject.Meta.Message = "Već postoji";
            //    return Json(resultObject);
            //}









            try
            {
                TryValidateModel(nastAktModel);

                _context.EvidencijeNastAktivnosti.Add(nastAktModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }




            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            //zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}