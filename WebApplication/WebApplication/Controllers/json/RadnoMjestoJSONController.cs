﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class RadnoMjestoJSONController : Controller
    {

        private readonly Context _context;

        public RadnoMjestoJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedRadnoMjesto> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class RadnoMjestoBody
        {
            public string Naziv { get; set; }
            public int NormaSati { get; set; }
        }

        public class SelectedRadnoMjesto
        {
            public int Id { get; set; }
            public string Naziv { get; set; }
            public int NormaSati { get; set; }
        }


        [HttpGet]
        public async Task<JsonResult> GetList()
        {
            List<RadnoMjesto> list = await _context.RadnaMjesta.ToAsyncEnumerable().ToList();

            List<SelectedRadnoMjesto> selectedList = (from radnoMjesto in list
                                                select new SelectedRadnoMjesto
                                                {
                                                    Id = radnoMjesto.Id,
                                                    Naziv = radnoMjesto.Naziv,
                                                    NormaSati = radnoMjesto.NormaSati
                                                }).ToList();

            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var radnoMjesto = await _context.RadnaMjesta.SingleOrDefaultAsync(m => m.Id == id);

            if (radnoMjesto == null)
            {
                return Json(resultObject);
            }

            _context.RadnaMjesta.Remove(radnoMjesto);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] RadnoMjestoBody radnoMjestoBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (radnoMjestoBody == null)
            {
                return Json(resultObject);
            }

            if (_context.RadnaMjesta.Any(m => m.Naziv == radnoMjestoBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var radnoMjestoModel = new RadnoMjesto
            {
                Naziv = radnoMjestoBody.Naziv,
                NormaSati = radnoMjestoBody.NormaSati
            };

            try
            {
                TryValidateModel(radnoMjestoModel);

                _context.RadnaMjesta.Add(radnoMjestoModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] RadnoMjestoBody radnoMjestoBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var radnoMjesto = await _context.RadnaMjesta.FirstOrDefaultAsync(m => m.Id == id);

            if (radnoMjesto == null)
            {
                return Json(resultObject);
            }

            radnoMjesto.Naziv = radnoMjestoBody.Naziv;
            radnoMjesto.NormaSati = radnoMjestoBody.NormaSati;

            try
            {
                TryValidateModel(radnoMjesto);

                _context.Entry(radnoMjesto).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}