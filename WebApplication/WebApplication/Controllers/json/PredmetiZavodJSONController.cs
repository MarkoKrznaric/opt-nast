﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredmetiZavodJSONController : Controller
    {

        private readonly Context _context;

        public PredmetiZavodJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/

        [TempData]
        public string ErrorMessage { get; set; }

        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }

        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedZavod> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class ZavodBody
        {
            public string Naziv { get; set; }
        }

        public class SelectedZavod
        {
            public int Id { get; set; }
            public string Sifra { get; set; }
            public string Naziv { get; set; }
            public string Studij { get; set; }
            public string AkGodina { get; set; }
            public string Semestar { get; set; }
            public string RasporedenoSati { get; set; }
            public string OdradenoSati { get; set; }
            public string UkupnoSati { get; set; }
        }


        [HttpGet]
        public JsonResult GetList(int odabirZavoda, int odabirAkGod)
        {

            /*ResultObject provjera = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = id },
                Data = new ResultObject.DataObject { }
            };
            return Json(provjera);*/
            //List<PredAkGodSemOsoba> list =  _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();

            //var idZavoda = _context.Osobe.FirstOrDefault(m => m.UserName.Equals(id)).IdZavod;

            List<Predmet> list = _context.Predmeti.Include(m => m.IdRazinaStudijNavigation).Where(m => m.IdZavod == odabirZavoda).ToList();
            //List<PredAkGodSemOsoba> list = _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).Where(m => m.IdOsoba.Equals(_context.Osobe.FirstOrDefault(x => x.UserName.Equals(id)).Id)).ToList();

            /*List<SelectedZavod> selectedList = (from predAkGodSemOsoba in list
                                                select new SelectedZavod
                                                {
                                                    Id = predAkGodSemOsoba.Id,
                                                    Naziv = zavod.Naziv
                                                }).ToList();*/

            List<SelectedZavod> selectedList = new List<SelectedZavod>();

            foreach(var pred in list)
            {
                List<PredAkGodSem> predAkGodSem = _context.PredAkGodSemestri.Where(m => m.IdPredmet == pred.Id && m.IdAkGodina == odabirAkGod).ToList();
                foreach(var p in predAkGodSem)
                {
                    var odrSati = 0.0;
                    var raspSati = 0.0;
                    var ukSati = 0.0;
                    List<PredAkGodSemOsoba> predAkGodSemOsoba = _context.PredAkGodSemOsobe.Where(m => m.IdPredAkGodSem == p.Id).ToList();
                    foreach(var o in predAkGodSemOsoba)
                    {
                        List<EvidNastAktivnosti> evid = _context.EvidencijeNastAktivnosti.Where(m => m.IdPredAkGodSemOsoba == o.Id).ToList();
                        foreach(var e in evid)
                        {
                            var idVrsteNast = _context.OptPredmeta.FirstOrDefault(m => m.Id == e.IdOptPred).IdVrstaNastave;
                            var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == idVrsteNast).IdTipNastave).Naziv;
                            if (tipNast.Equals("Predavanja"))
                            {
                                if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                                {
                                    odrSati = odrSati + e.SkoSati * 3;
                                }
                                else
                                {
                                    odrSati = odrSati + e.SkoSati * 2;
                                }

                            }
                            if (tipNast.Equals("Vježbe"))
                            {
                                odrSati = odrSati + e.SkoSati;
                            }
                            if (tipNast.Equals("Seminar"))
                            {
                                odrSati = odrSati + e.SkoSati * 1.5;
                            }
                        }
                    }

                    List<PredAkGodSemGrupa> predAkGodSemGrupa = _context.PredAkGodSemGrupe.Where(m => m.IdPredAkGodSem == p.Id).ToList();
                    foreach(var g in predAkGodSemGrupa)
                    {
                        var optPredSatiUk = _context.OptPredmeta.FirstOrDefault(m => m.IdPredmet == pred.Id && m.IdVrstaNastave == g.IdVrNastave).SatiUk;
                        var tipNast = _context.TipoviNastave.FirstOrDefault(m => m.Id == _context.VrsteNastave.FirstOrDefault(x => x.Id == g.IdVrNastave).IdTipNastave).Naziv;
                        if (tipNast.Equals("Predavanja"))
                        {
                            if (_context.RazineStudija.FirstOrDefault(m => m.Id == _context.Predmeti.FirstOrDefault(x => x.Id == p.IdPredmet).IdRazinaStudij).Naziv == "Poslijediplomski")
                            {
                                ukSati = ukSati + optPredSatiUk * 3;
                                if (g.IdOsoba != 1.ToString())
                                    raspSati = raspSati + optPredSatiUk * 3;
                            }
                            else
                            {
                                ukSati = ukSati + optPredSatiUk * 2;
                                if (g.IdOsoba != 1.ToString())
                                    raspSati = raspSati + optPredSatiUk * 2;
                            }

                        }
                        if (tipNast.Equals("Vježbe"))
                        {
                            ukSati = ukSati + optPredSatiUk;
                            if (g.IdOsoba != 1.ToString())
                                raspSati = raspSati + optPredSatiUk;
                        }
                        if (tipNast.Equals("Seminar"))
                        {
                            ukSati = ukSati + optPredSatiUk * 1.5;
                            if (g.IdOsoba != 1.ToString())
                                raspSati = raspSati + optPredSatiUk * 1.5;
                        }
                    }


                    SelectedZavod dodaj = new SelectedZavod
                    {
                        Id = pred.Id,
                        Sifra = pred.Sifra.ToString(),
                        Naziv = pred.Naziv,
                        Studij = (pred.IdRazinaStudijNavigation == null) ? "" : pred.IdRazinaStudijNavigation.Naziv,
                        //AkGodina = _context.AkademskeGodine.FirstOrDefault(m => m.Id == p.IdAkGodina).Godina.ToString(),
                        Semestar = _context.Semestri.FirstOrDefault(m => m.Id == p.IdSemestar).Naziv,
                        OdradenoSati = odrSati.ToString(),
                        RasporedenoSati = raspSati.ToString(),
                        UkupnoSati = ukSati.ToString()
                    };
                    selectedList.Add(dodaj);
                }
            }
            /*List<SelectedZavod> selectedList = (from predmeti in list
                                                select new SelectedZavod
                                                {
                                                    Id = predmeti.Id,
                                                    Sifra = predmeti.Sifra.ToString(),
                                                    Studij = (predmeti.IdRazinaStudijNavigation == null) ? "" : predmeti.IdRazinaStudijNavigation.Naziv,
                                                    RasporedenoSati = (predAkGodSemOsoba.IdUlogaNaPredNavigation == null) ? "" : predAkGodSemOsoba.IdUlogaNaPredNavigation.Naziv,
                                                    OdradenoSati = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.AkademskeGodine.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdAkGodina).Godina.ToString(),
                                                    UkupnoSati = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdSemestar).Naziv,
                                                }).ToList();*/


            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.SingleOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            _context.Zavodi.Remove(zavod);
            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ZavodBody zavodBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Greška" },
                Data = new ResultObject.DataObject { }
            };
            if (zavodBody == null)
            {
                return Json(resultObject);
            }

            if (_context.Zavodi.Any(m => m.Naziv == zavodBody.Naziv))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Već postoji";
                return Json(resultObject);
            }

            var zavodModel = new Zavod
            {
                Naziv = zavodBody.Naziv
            };

            try
            {
                TryValidateModel(zavodModel);

                _context.Zavodi.Add(zavodModel);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška";
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] ZavodBody vrstaArtiklaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađenoPut" },
                Data = new ResultObject.DataObject { }
            };

            var zavod = await _context.Zavodi.FirstOrDefaultAsync(m => m.Id == id);

            if (zavod == null)
            {
                return Json(resultObject);
            }

            zavod.Naziv = vrstaArtiklaBody.Naziv;

            try
            {
                TryValidateModel(zavod);

                _context.Entry(zavod).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}