﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers.json
{
    [Route("[controller]/[action]")]
    public class PredAkGodSemOsobaJSONController : Controller
    {

        private readonly Context _context;

        public PredAkGodSemOsobaJSONController(Context context)
        {
            _context = context;
        }
        /*
        public IActionResult Index()
        {
            return View();
        }*/
        public class PredAkGodSemOsobaBody
        {
            public string IdOsoba { get; set; }
            public int IdUlogaNaPred { get; set; }
            public int IdPredmet { get; set; }
            public int IdAkGodina { get; set; }
            public int IdSemestar { get; set; }
        }
        public class ListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSemOsoba> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class ResultObject
        {
            public class DataObject { }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        public class SelectedPredAkGodSemOsoba
        {
            public int Id { get; set; }
            public string Osoba { get; set; }
            public string UlogaNaPred { get; set; }
            public string Predmet { get; set; }
            public string AkGodina { get; set; }
            public string Semestar { get; set; }
        }
        public class MetaObject
        {
            public int Error { get; set; }
            public string Message { get; set; }
        }
        public class PredAkGodSemOsobaListObject
        {
            public class DataObject
            {
                public List<SelectedPredAkGodSemOsoba> List { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }
        [TempData]
        public string ErrorMessage { get; set; }

        /*tu sam stao*/
        [HttpGet]
        public async Task<JsonResult> GetList()
        {

            List<PredAkGodSemOsoba> list = await _context.PredAkGodSemOsobe.Include(m => m.IdOsobaNavigation).Include(m => m.IdUlogaNaPredNavigation).Include(m => m.IdPredAkGodSemNavigation).ToListAsync();

            /*List<SelectedPredAkGodSemOsoba> selectedList = (from predAkGodSemOsoba in list
                                                            select new SelectedPredAkGodSemOsoba
                                                            {
                                                                Id = predAkGodSemOsoba.Id,
                                                                Osoba = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : predAkGodSemOsoba.IdOsobaNavigation.Ime,
                                                                UlogaNaPred = (predAkGodSemOsoba.IdUlogaNaPredNavigation == null) ? "" : predAkGodSemOsoba.IdUlogaNaPredNavigation.Naziv,
                                                                AkGodina = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdAkGodinaNavigation.Godina.ToString(),
                                                                Predmet = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmetNavigation.Naziv,
                                                                Semestar = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : predAkGodSemOsoba.IdPredAkGodSemNavigation.IdSemestarNavigation.Naziv,
                                                            }).ToList();*/

            List<SelectedPredAkGodSemOsoba> selectedList = (from predAkGodSemOsoba in list
                                                            select new SelectedPredAkGodSemOsoba
                                                            {
                                                                Id = predAkGodSemOsoba.Id,
                                                                Osoba = (predAkGodSemOsoba.IdOsobaNavigation == null) ? "" : (predAkGodSemOsoba.IdOsobaNavigation.Ime + " " + predAkGodSemOsoba.IdOsobaNavigation.Prezime),
                                                                UlogaNaPred = (predAkGodSemOsoba.IdUlogaNaPredNavigation == null) ? "" : predAkGodSemOsoba.IdUlogaNaPredNavigation.Naziv,
                                                                AkGodina = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.AkademskeGodine.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdAkGodina).Godina.ToString(),
                                                                Predmet = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Predmeti.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdPredmet).Naziv,
                                                                Semestar = (predAkGodSemOsoba.IdPredAkGodSemNavigation == null) ? "" : _context.Semestri.FirstOrDefault(m => m.Id == predAkGodSemOsoba.IdPredAkGodSemNavigation.IdSemestar).Naziv,
                                                            }).ToList();



            ListObject resultObject = new ListObject
            {
                Meta = new MetaObject { },
                Data = new ListObject.DataObject { List = selectedList }
            };

            return Json(resultObject);
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] short id)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predAkGodSemOsoba = await _context.PredAkGodSemOsobe.SingleOrDefaultAsync(m => m.Id == id);



            if (predAkGodSemOsoba == null)
            {
                return Json(resultObject);
            }

            _context.PredAkGodSemOsobe.Remove(predAkGodSemOsoba);

            await _context.SaveChangesAsync();

            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }


        [HttpPost]
        public async Task<JsonResult> Post([FromBody] PredAkGodSemOsobaBody predAkGodSemOsobaBody)
        {
            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 400, Message = "Naletio na Postt" },
                Data = new ResultObject.DataObject { }
            };
            if (predAkGodSemOsobaBody == null)
            {
                return Json(resultObject);
            }
            var idZaProvjerit = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar).Id;
            if (_context.PredAkGodSemOsobe.Any(m => m.IdOsoba.ToString() == predAkGodSemOsobaBody.IdOsoba && m.IdPredAkGodSem == idZaProvjerit))
            {
                resultObject.Meta.Error = 400;
                resultObject.Meta.Message = "Odabrana osoba već predaje na odabranom predmetu";
                return Json(resultObject);
            }
            if(!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar))
            {
                resultObject.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(resultObject);
            }
            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar).Id;

            var predAkGodSemOsobaModel = new PredAkGodSemOsoba
            {

                IdOsoba = predAkGodSemOsobaBody.IdOsoba,
                IdOsobaNavigation = _context.Osobe.FirstOrDefault(m => m.Id == predAkGodSemOsobaBody.IdOsoba),

                IdUlogaNaPred = predAkGodSemOsobaBody.IdUlogaNaPred,
                IdUlogaNaPredNavigation = _context.UlogeNaPred.FirstOrDefault(m => m.Id == predAkGodSemOsobaBody.IdUlogaNaPred),

                IdPredAkGodSem = idZaPredAkGodSem,
                IdPredAkGodSemNavigation = _context.PredAkGodSemestri.FirstOrDefault(m => m.Id == idZaPredAkGodSem)
            };


            try
            {
                TryValidateModel(predAkGodSemOsobaModel);

                _context.PredAkGodSemOsobe.Add(predAkGodSemOsobaModel);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException /* ex */)
            {
                resultObject.Meta.Message = "Greška2";
                return Json(resultObject);
            }

            


            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] short id, [FromBody] PredAkGodSemOsobaBody predAkGodSemOsobaBody)
        {

            ResultObject resultObject = new ResultObject
            {
                Meta = new MetaObject { Error = 404, Message = "Nije pronađeno" },
                Data = new ResultObject.DataObject { }
            };

            var predAkGodSemOsoba = await _context.PredAkGodSemOsobe.FirstOrDefaultAsync(m => m.Id == id);
            //var predAkGodSem = _context.PredAkGodSemestri.Include(m => m.IdAkGodinaNavigation)
            //   .Include(m => m.IdSemestarNavigation)
            //   .Where(m => m.IdPredmet == id && m.IdAkGodinaNavigation.Godina == predmetBody.AkGod && m.IdSemestarNavigation.Naziv == predmetBody.Semestar);
            if (!_context.PredAkGodSemestri.Any(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar))
            {
                resultObject.Meta.Message = "Predmet ne postoji u odabranoj akademskoj godini i semestru";
                return Json(resultObject);
            }
            var idZaPredAkGodSem = _context.PredAkGodSemestri.FirstOrDefault(m => m.IdAkGodina == predAkGodSemOsobaBody.IdAkGodina && m.IdPredmet == predAkGodSemOsobaBody.IdPredmet && m.IdSemestar == predAkGodSemOsobaBody.IdSemestar).Id;

            

            if (predAkGodSemOsoba == null)
            {
                return Json(resultObject);
            }

            predAkGodSemOsoba.IdOsoba = predAkGodSemOsobaBody.IdOsoba;
            predAkGodSemOsoba.IdOsobaNavigation = _context.Osobe.FirstOrDefault(m => m.Id == predAkGodSemOsobaBody.IdOsoba);

            predAkGodSemOsoba.IdUlogaNaPred = predAkGodSemOsobaBody.IdUlogaNaPred;
            predAkGodSemOsoba.IdUlogaNaPredNavigation = _context.UlogeNaPred.FirstOrDefault(m => m.Id == predAkGodSemOsobaBody.IdUlogaNaPred);

            predAkGodSemOsoba.IdPredAkGodSem = idZaPredAkGodSem;
            predAkGodSemOsoba.IdPredAkGodSemNavigation = _context.PredAkGodSemestri.FirstOrDefault(m => m.Id == idZaPredAkGodSem);

            try
            {
                TryValidateModel(predAkGodSemOsoba);
                //TryValidateModel(p);

                _context.Entry(predAkGodSemOsoba).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                resultObject.Meta = new MetaObject { Error = 400, Message = "Greška" };
                return Json(resultObject);
            }


            resultObject.Meta = new MetaObject { };
            return Json(resultObject);
        }
    }
}

