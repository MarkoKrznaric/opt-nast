﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<Osoba> _userManager;
        private readonly SignInManager<Osoba> _signInManager;
        private readonly ILogger _logger;

        public AccountController(
            UserManager<Osoba> userManager,
            SignInManager<Osoba> signInManager,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        public class ReturnObject
        {
            public class DataObject
            {
                public string Id { get; set; }
                public string Role { get; set; }
            }
            public class MetaObject
            {
                public int Error { get; set; }
                public string Message { get; set; }
            }
            public MetaObject Meta { get; set; }
            public DataObject Data { get; set; }
        }

        public class RegisterBody
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Role { get; set; }
            public string Mail { get; set; }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> Authentification(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                ReturnObject resultObject = new ReturnObject
                {
                    Meta = new ReturnObject.MetaObject { Error = 404, Message = "Failed to authenticate." },
                    Data = new ReturnObject.DataObject { }
                };
                return Json(resultObject);
            }
            var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in.");
                var claims = await _userManager.GetClaimsAsync(user);
                var role = "";
                if (claims.Count > 0)
                {
                    role = claims.FirstOrDefault().Value.ToString();
                }
                ReturnObject resultObject = new ReturnObject
                {
                    Meta = new ReturnObject.MetaObject { },
                    Data = new ReturnObject.DataObject
                    {
                        Id = user.Id,
                        Role = role
                    }
                };
                return Json(resultObject);
            }
            else
            {
                ReturnObject resultObject = new ReturnObject
                {
                    Meta = new ReturnObject.MetaObject { Error = 404, Message = "Failed to authenticate." },
                    Data = new ReturnObject.DataObject { }
                };
                return Json(resultObject);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> Register([FromBody] RegisterBody registerBody)
        {
            ReturnObject resultObject = new ReturnObject
            {
                Meta = new ReturnObject.MetaObject { Error = 404, Message = "No body found" },
                Data = new ReturnObject.DataObject { }
            };
            if (registerBody == null)
            {

                return Json(resultObject);
            }
            var user = new Osoba
            {
                UserName = registerBody.Username,
                Ime = registerBody.Ime,
                Prezime = registerBody.Prezime,
                Mail = registerBody.Mail,
            };
            var result = await _userManager.CreateAsync(user, registerBody.Password);
            if (result.Succeeded)
            {
                _logger.LogInformation("User created a new account with password.");

                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, registerBody.Role));

                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation("User created a new account with password.");

                resultObject.Meta = new ReturnObject.MetaObject { };
                return Json(resultObject);
            }

            resultObject.Meta.Error = 400;
            resultObject.Meta.Message = "Bad Request";
            resultObject.Data = new ReturnObject.DataObject { Role = registerBody.Role };
            return Json(resultObject);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> DeleteAllUsers()
        {
            var users = _userManager.Users.ToList();
            foreach (var user in users)
            {
                await _userManager.DeleteAsync(user);
            }
            return Ok();
        }

        [HttpPost]
        public async Task<JsonResult> Logout()
        {
            ReturnObject resultObject = new ReturnObject
            {
                Meta = new ReturnObject.MetaObject { },
                Data = new ReturnObject.DataObject { }
            };
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return Json(resultObject);
        }



    }
}
