﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public static class DbInitializer
    {

        public static async Task Initialize(Context context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Uloge.Any())
            {
                return;   // DB has been seeded
            }

            var uloge = new Uloga[]
            {
            new Uloga{Naziv="Administrator"}
            };
            foreach (Uloga u in uloge)
            {
                context.Uloge.Add(u);
            }
            context.SaveChanges();

        }
    }
}
