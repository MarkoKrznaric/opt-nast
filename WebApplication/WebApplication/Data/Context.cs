﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Models
{
    public class Context : IdentityDbContext<Osoba>
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<TipNastave> TipoviNastave { get; set; }
        public DbSet<VrstaNastave> VrsteNastave { get; set; }
        public DbSet<OptPred> OptPredmeta { get; set; }
        public DbSet<Predmet> Predmeti { get; set; }
        public DbSet<Osoba> Osobe { get; set; }
        public DbSet<Uloga> Uloge { get; set; }
        public DbSet<RadnoMjesto> RadnaMjesta { get; set; }
        public DbSet<Zavod> Zavodi { get; set; }
        public DbSet<RazinaStudij> RazineStudija { get; set; }
        public DbSet<UlogaNaPred> UlogeNaPred { get; set; }
        public DbSet<PredAkGodSemOsoba> PredAkGodSemOsobe { get; set; }
        public DbSet<PredAkGodSemGrupa> PredAkGodSemGrupe { get; set; }
        public DbSet<Grupa> Grupe { get; set; }
        public DbSet<PredAkGodSem> PredAkGodSemestri { get; set; }
        public DbSet<AkademskaGodina> AkademskeGodine { get; set; }
        public DbSet<Semestar> Semestri { get; set; }
        public DbSet<EvidNastAktivnosti> EvidencijeNastAktivnosti { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<TipNastave>().ToTable("TipNastave");
            builder.Entity<VrstaNastave>().ToTable("VrstaNastave");
            builder.Entity<OptPred>().ToTable("OptPred");
            builder.Entity<Predmet>().ToTable("Predmet");
            builder.Entity<Osoba>().ToTable("Osoba");
            builder.Entity<Uloga>().ToTable("Uloga");
            builder.Entity<RadnoMjesto>().ToTable("RadnoMjesto");
            builder.Entity<Zavod>().ToTable("Zavod");
            builder.Entity<RazinaStudij>().ToTable("RazinaStudij");
            builder.Entity<UlogaNaPred>().ToTable("UlogaNaPred");
            builder.Entity<PredAkGodSemOsoba>().ToTable("PredAkGodSemOsoba");
            builder.Entity<PredAkGodSemGrupa>().ToTable("PredAkGodSemGrupa");
            builder.Entity<Grupa>().ToTable("Grupa");
            builder.Entity<PredAkGodSem>().ToTable("PredAkGodSem");
            builder.Entity<AkademskaGodina>().ToTable("AkademskaGodina");
            builder.Entity<Semestar>().ToTable("Semestar");
            builder.Entity<EvidNastAktivnosti>().ToTable("EvidNastAktivnosti");
        }
    }
}
