﻿﻿const path = require('path');

module.exports = {
    entry: './Scripts/main.jsx',
    output: {
        filename: 'main.build.js',
        path: path.resolve(__dirname, 'wwwroot/js/'),
        publicPath: "/js/"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};