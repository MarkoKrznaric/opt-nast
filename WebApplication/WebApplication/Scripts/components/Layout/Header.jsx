﻿import React from 'react';
import {
    NavLink
} from 'react-router-dom';
import { DropdownMenu, MenuItem } from 'react-bootstrap-dropdown-menu';



class Header extends React.Component {
    constructor() {
        super();
        this.handleClickLogOff = this.handleClickLogOff.bind(this);
    }

    handleClickLogOff() {
        const xhr = new XMLHttpRequest();
        const url = "/Account/Logout/";
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                this.props.changeLogState('Guest', '');
            }
        };
        const data = JSON.stringify({});
        xhr.send(data);
    }

    render() {

        return (
            <header>
                <div className="container">
                    <ul className="nav navbar-nav">
                        {this.props.role === 'Administrator' && (
                            <DropdownMenu triggerType='text' trigger='Podaci'>
                                <NavLink to='/Osobe' className="navLink">Osobe</NavLink>
                                <NavLink to='/Zavodi' className="navLink">Zavodi</NavLink>
                                <NavLink to='/RadnaMjesta' className="navLink">Radna mjesta</NavLink>
                                <NavLink to='/Predmeti' className="navLink">Predmeti</NavLink>
                                <NavLink to='/VrsteNastave' className="navLink">Vrste nastave</NavLink>
                                <NavLink to='/OpterecenjaPredmeta' className="navLink">Opterecenja predmeta</NavLink>
                                <NavLink to='/PredAkGodSemestri' className="navLink">Predmet u akademskoj godini i semestru</NavLink>
                                <NavLink to='/PredAkGodSemOsobe' className="navLink">Nastavnik na predmetu u akGodini i semestru</NavLink>
                                <NavLink to='/PredAkGodSemGrupe' className="navLink">Nastavnik – izvođač grupe iz predmeta</NavLink>
                            </DropdownMenu>
                        )}
                        {this.props.role === 'Administrator' && (
                            <li>
                                <NavLink to='/Opterecenje' className="navLink">Izvješće</NavLink>
                            </li>
                        )}
                        
                        {this.props.role === 'Voditelj zavoda' && (
                            <li>
                                <NavLink to='/PredmetiNastavnika' className="navLink">Evidencija nastavnih aktivnosti</NavLink>
                            </li>
                        )}
                        {this.props.role === 'Voditelj zavoda' && (
                            <li>
                                <NavLink to='/Opterecenje' className="navLink">Izvješće</NavLink>
                            </li>
                        )}
                        {this.props.role === 'Voditelj zavoda' && (
                            <li>
                                <NavLink to='/MojZavod' className="navLink">Pregled podataka po zavodu</NavLink>
                            </li>
                        )}
                        {this.props.role === 'Voditelj zavoda' && (
                            <li>
                                <NavLink to='/OsobneInformacije' className="navLink">Osobne informacije</NavLink>
                            </li>
                        )}
                        
                        {this.props.role === 'Nastavnik' && (
                            <li>
                                <NavLink to='/Opterecenje' className="navLink">Izvješće</NavLink>
                            </li>
                        )}
                        {this.props.role === 'Nastavnik' && (
                            <li>
                                <NavLink to='/OsobneInformacije' className="navLink">Osobne informacije</NavLink>
                            </li>
                        )}
                        {this.props.role === 'Nastavnik' && (
                            <li>
                                <NavLink to='/PredmetiNastavnika' className="navLink">Evidencija nastavnih aktivnosti</NavLink>
                            </li>
                        )}
                    </ul>
                    {this.props.role !== '' && (

                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <a role="button"><span className="fa fa-user fa-fw"></span> {this.props.role}</a>
                            </li>
                            <li>
                                <a role="button" onClick={this.handleClickLogOff}>Odjava<span className="fa fa-sign-out fa-fw"></span></a>
                            </li>
                        </ul>
                    )}
                </div>
            </header>
        );
    }
}

export default Header;