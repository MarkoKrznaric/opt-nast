﻿
import React from 'react';
import ReactTable from 'react-table';

import 'react-table/react-table.css';

import PredmetSelect from './PredAkGodSem/PredmetSelect';
import AkGodSelect from './PredAkGodSem/AkGodSelect';
import SemestarSelect from './PredAkGodSem/SemestarSelect';
import OsobaSelect from './PredAkGodSemOsoba/OsobaSelect';
import GrupaSelect from './PredAkGodSemGrupa/GrupaSelect';
import VrstaNastaveSelect from './PredAkGodSemGrupa/VrstaNastaveSelect';

class PredAkGodSemGrupe extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                idGrupa: null,
                idVrNastave: null,
                idOsoba: null,
                idAkGodina: null,
                idSemestar: null,
                idPredmet: null
            },
            rows: []
        };
        this.statetest = this.statetest.bind(this);

        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/PredAkGodSemGrupaJSON/GetList";

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({
                        loading: false,
                        rows: json.data.list
                    });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        xhr.send();
    }

    requestPost(idGrupa = 0, idVrNastave = 0, idOsoba = 0, idPredmet = 0, idAkGodina = 0, idSemestar = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/PredAkGodSemGrupaJSON/Post";

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "IdGrupa": idGrupa,
            "IdVrNastave": idVrNastave,
            "IdOsoba": idOsoba,
            "IdPredmet": idPredmet,
            "IdAkGodina": idAkGodina,
            "IdSemestar": idSemestar
        });
        xhr.send(data);
    }

    requestDelete(id) {
        const xhr = new XMLHttpRequest();
        const url = "/PredAkGodSemGrupaJSON/Delete/" + id;

        xhr.open("DELETE", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        }
        xhr.send();
    }

    requestPut(id, idGrupa = 0, idVrNastave = 0, idOsoba = 0, idPredmet = 0, idAkGodina = 0, idSemestar = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/PredAkGodSemGrupaJSON/Put/" + id;

        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.requestList();
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "IdGrupa": idGrupa,
            "IdVrNastave": idVrNastave,
            "IdOsoba": idOsoba,
            "IdPredmet": idPredmet,
            "IdAkGodina": idAkGodina,
            "IdSemestar": idSemestar
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }
        const emptyData = {
            idGrupa: null,
            idVrNastave: null,
            idOsoba: null,
            idAkGodina: null,
            idSemestar: null,
            idPredmet: null
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            idGrupa: "",
            idVrNastave: "",
            idOsoba: "",
            akGodina: "",
            semestar: "",
            predmet: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {
        const columns = [{
            Header: 'Grupa',
            accessor: 'grupa',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <GrupaSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'VrNastave',
            accessor: 'vrNastave',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <VrstaNastaveSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Osoba',
            accessor: 'osoba',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <OsobaSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Predmet',
            accessor: 'predmet',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <PredmetSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'AkGodina',
            accessor: 'akGodina',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <AkGodSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Semestar',
            accessor: 'semestar',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <SemestarSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Alati',
            show: !this.props.disableTools,
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.idGrupa,
                                            this.state.editingData.idVrNastave,
                                            this.state.editingData.idOsoba,
                                            this.state.editingData.idPredmet,
                                            this.state.editingData.idAkGodina,
                                            this.state.editingData.idSemestar
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.idGrupa,
                                            this.state.editingData.idVrNastave,
                                            this.state.editingData.idOsoba,
                                            this.state.editingData.idPredmet,
                                            this.state.editingData.idAkGodina,
                                            this.state.editingData.idSemestar
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];

        return (
            <div className="container">
                <h1>Nastavnik - izvođač grupe iz predmeta</h1>

                <br />
                {!this.props.disableTools && (
                    <a onClick={this.handleAdd} className="btn btn-success">Dodaj</a>
                )}
                <br /><br />

                <ReactTable
                    data={this.state.rows}
                    columns={columns}
                    filterable={true}
                    loading={this.state.loading}
                    defaultPageSize={5}
                    className="-striped -highlight"
                    previousText='Prethodna'
                    nextText='Sljedeća'
                    loadingText='Učitavanje...'
                    noDataText='Nema pronađenih redaka'
                    pageText='Stranica'
                    ofText='od'
                    rowsText='redaka'
                />

            </div>
        );
    }
}

export default PredAkGodSemGrupe;