﻿import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class PredmetiZavoda extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                sifra: "",
                naziv: "",
                studij: "",
                semestar: "",
                odradenoSati: "",
                rasporedenoSati: "",
                ukupnoSati: "",
            },
            rows: []
        };
        
        this.statetest = this.statetest.bind(this);

        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }


    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/PredmetiZavodJSON/GetList/?odabirZavoda=" + this.props.odabirZavoda + "&odabirAkGod=" + this.props.odabirAkGod;

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({
                        loading: false,
                        rows: json.data.list
                    });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        xhr.send();
    }

    requestPost(naziv) {
        const xhr = new XMLHttpRequest();
        const url = "/ZavodJSON/Post";

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "Naziv": naziv
        });
        xhr.send(data);
    }

    requestDelete(id) {
        const xhr = new XMLHttpRequest();
        const url = "/ZavodJSON/Delete/" + id;

        xhr.open("DELETE", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        }
        xhr.send();
    }

    requestPut(id, naziv) {
        const xhr = new XMLHttpRequest();
        const url = "/ZavodJSON/Put/" + id;

        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.requestList();
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "Naziv": naziv
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }
        const emptyData = {
            naziv: ""
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            naziv: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {
        const divStyle = {
            color: 'red',
        };
        const divStyleGreen = {
            color: 'green',
        };
        var x;
        const columns = [{
            Header: 'Sifra',
            accessor: 'sifra',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.sifra = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                            
                        )}
                    
                </div>
            )
        }, {
            Header: 'Naziv',
            accessor: 'naziv',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.naziv = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Studij',
            accessor: 'studij',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.studij = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Semestar',
            accessor: 'semestar',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.semestar = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'RasporedenoSati',
            accessor: 'rasporedenoSati',
            Cell: row => (
                <div>
                    {parseInt(row.value) < parseInt(row.original.ukupnoSati) ? (
                        <div style={divStyle}>{row.value}</div>
                    ) : (
                            <div style={divStyleGreen}>{row.value}</div>
                        )}
                </div>
            )
        }, {
            Header: 'OdradenoSati',
            accessor: 'odradenoSati',
            Cell: row => (
                <div>
                    {parseInt(row.value) < parseInt(row.original.rasporedenoSati) ? (
                        <div style={divStyle}>{row.value}</div>
                    ) : (
                            <div style={divStyleGreen}>{row.value}</div>
                        )}
                </div>
            )
        }, {
            Header: 'UkupnoSati',
            accessor: 'ukupnoSati',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.ukupnoSati = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
            }, {
                Header: 'Alati',
                filterable: false,
                Cell: row => {
                    return (
                        <div align="center">
                            <a onClick={() => {
                                this.props.changePredmetiState(this.props.odabirAkGod, row.original.naziv, row.original.semestar);
                                this.props.changeLinkPredmeti();
                            }} className="btn btn-success"><em className="fa fa-sign-in" ></em></a>
                        </div>
                    );
                }
            }];

        return (
            <div className="container">

                <br />
                {!this.props.disableTools && (
                    <a onClick={this.handleAdd} className="btn btn-success">Dodaj zavod<em className="fa fa-plus"></em></a>
                )}
                <br /><br />

                <ReactTable
                    data={this.state.rows}
                    columns={columns}
                    filterable={true}
                    loading={this.state.loading}
                    defaultPageSize={5}
                    className="-striped -highlight"
                    previousText='Prethodna'
                    nextText='Sljedeća'
                    loadingText='Učitavanje...'
                    noDataText='Nema pronađenih redaka'
                    pageText='Stranica'
                    ofText='od'
                    rowsText='redaka'
                />

            </div>
        );
    }
}
export default PredmetiZavoda;