﻿import React from 'react';

import LoginForm from './Home/LoginForm';
import RegisterForm from './Home/RegisterForm';

class Home extends React.Component {
    render() {
        return (
            <div>
                <div className="jumbotron">
                    <div className="overlay"></div>
                    <div className="user-handle">
                        {this.props.username === 'Guest' &&
                            (
                                <LoginForm changeLogState={this.props.changeLogState} />
                            )}
                    </div>
                </div>
            </div>
        );

    }
}

export default Home;