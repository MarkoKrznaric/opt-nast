﻿import React from 'react';
import ReactTable from 'react-table';

import 'react-table/react-table.css';

import UlogaSelect from './Osoba/UlogaSelect';
import RadnoMjestoSelect from './Osoba/RadnoMjestoSelect';
import ZavodSelect from './Osoba/ZavodSelect';

class Osobe extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                username: null,
                password: null,
                ime: null,
                prezime: null,
                mail: null,
                idRadnoMjesto: null,
                uloga: null,
                idZavod: null
            },
            rows: []
        };
        this.statetest = this.statetest.bind(this);

        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/GetList";

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({
                        loading: false,
                        rows: json.data.list
                    });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        xhr.send();
    }

    requestPost(username, password, ime, prezime, uloga, mail, idRadnoMjesto=0, idZavod=0) {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/Post";


        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Zapis uspješno unesen");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "Username": username,
            "Password": password,
            "Ime": ime,
            "Prezime": prezime,
            "Uloga": uloga,
            "Mail": mail,
            "IdRadnoMjesto": idRadnoMjesto,
            "IdZavod": idZavod,
        });
        xhr.send(data);
    }

    requestDelete(id) {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/Delete/" + id;
        xhr.open("DELETE", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        }
        xhr.send();
    }

    requestPut(id, username, password, ime, prezime, uloga = 0, mail, idRadnoMjesto=0, idZavod=0) {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/Put/" + id;


        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.requestList();
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "Username": username,
            "Password": password,
            "Ime": ime,
            "Prezime": prezime,
            "Uloga": uloga,
            "Mail": mail,
            "IdRadnoMjesto": idRadnoMjesto,
            "IdZavod": idZavod,
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }

        const emptyData = {
            username: null,
            password: null,
            ime: null,
            prezime: null,
            mail: null,
            idRadnoMjesto: null,
            uloga: null,
            idZavod: null
        };

        const addedRow = this.state.rows;
        addedRow.unshift({
            username: "",
            password: "",
            ime: "",
            prezime: "",
            mail: "",
            radnoMjesto: "",
            uloga: "",
            zavod: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {

        const columns = [{
            Header: 'Korisničko ime',
            accessor: 'username',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.username = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Lozinka',
            accessor: 'password',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.password = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        ></div>
                    ) : (
                            <p>Hashed</p>
                        )}
                </div>
            )
        }, {
            Header: 'Ime',
            accessor: 'ime',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.ime = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Prezime',
            accessor: 'prezime',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.prezime = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Mail',
            accessor: 'mail',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.mail = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
            }, {
                Header: 'RadnoMjesto',
                accessor: 'radnoMjesto',
                style: { overflow: 'initial' },
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <RadnoMjestoSelect changeEditingRow={this.changeEditingRow} />
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'Uloga',
                accessor: 'uloga',
                style: { overflow: 'initial' },
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <UlogaSelect uloga={row.value} changeEditingRow={this.changeEditingRow} />
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'Zavod',
                accessor: 'zavod',
                style: { overflow: 'initial' },
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <ZavodSelect changeEditingRow={this.changeEditingRow} />
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
            Header: 'Alati',
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.username,
                                            this.state.editingData.password,
                                            this.state.editingData.ime,
                                            this.state.editingData.prezime,
                                            this.state.editingData.uloga,
                                            this.state.editingData.mail,
                                            this.state.editingData.idRadnoMjesto,
                                            this.state.editingData.idZavod
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.username,
                                            this.state.editingData.password,
                                            this.state.editingData.ime,
                                            this.state.editingData.prezime,
                                            this.state.editingData.uloga,
                                            this.state.editingData.mail,
                                            this.state.editingData.idRadnoMjesto,
                                            this.state.editingData.idZavod
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;
                                        data.username = row.original.username;
                                        data.password = row.original.password;
                                        data.ime = row.original.ime;
                                        data.prezime = row.original.prezime;
                                        data.mail = row.original.mail;
                                        data.uloga = row.original.uloga;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];

        return (
            <div className="container">
                <div className="row">
                    <h1>Osobe</h1>

                    <br />
                    <a onClick={this.handleAdd} className="btn btn-success">Dodaj</a>
                    <br /><br />

                    <ReactTable
                        data={this.state.rows}
                        columns={columns}
                        filterable={true}
                        loading={this.state.loading}
                        defaultPageSize={5}
                        className="-striped -highlight"
                        previousText='Prethodna'
                        nextText='Sljedeća'
                        loadingText='Učitavanje...'
                        noDataText='Nema pronađenih redaka'
                        pageText='Stranica'
                        ofText='od'
                        rowsText='redaka'
                    />
                </div>
            </div>
        );
    }
}

export default Osobe;