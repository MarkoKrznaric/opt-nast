﻿import React from 'react';
import ReactTable from 'react-table';

import 'react-table/react-table.css';

import TipNastaveSelect from './VrstaNastave/TipNastaveSelect';

class VrsteNastave extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                naziv: null,
                idTipNastave: null
            },
            rows: []
        };
        this.statetest = this.statetest.bind(this);

        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/VrstaNastaveJSON/GetList";

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({
                        loading: false,
                        rows: json.data.list
                    });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        xhr.send();
    }

    requestPost(naziv, idTipNastave = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/VrstaNastaveJSON/Post";

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "Naziv": naziv,
            "IdTipNastave": idTipNastave
        });
        xhr.send(data);
    }

    requestDelete(id) {
        const xhr = new XMLHttpRequest();
        const url = "/VrstaNastaveJSON/Delete/" + id;

        xhr.open("DELETE", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        }
        xhr.send();
    }

    requestPut(id, naziv, idTipNastave = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/VrstaNastaveJSON/Put/" + id;

        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.requestList();
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "Naziv": naziv,
            "IdTipNastave": idTipNastave,
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }
        const emptyData = {
            naziv: null,
            idTipNastave: null
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            naziv: "",
            tipNastave: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {
        const columns = [{
            Header: 'Naziv',
            accessor: 'naziv',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.naziv = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'TipNastave',
            accessor: 'tipNastave',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <TipNastaveSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Alati',
            show: !this.props.disableTools,
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.naziv,
                                            this.state.editingData.idTipNastave,
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.naziv,
                                            this.state.editingData.idTipNastave
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;
                                        data.naziv = row.original.naziv;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];

        return (
            <div className="container">
                <h1>Vrste nastave</h1>

                <br />
                {!this.props.disableTools && (
                    <a onClick={this.handleAdd} className="btn btn-success">Dodaj</a>
                )}
                <br /><br />

                <ReactTable
                    data={this.state.rows}
                    columns={columns}
                    filterable={true}
                    loading={this.state.loading}
                    defaultPageSize={5}
                    className="-striped -highlight"
                    previousText='Prethodna'
                    nextText='Sljedeća'
                    loadingText='Učitavanje...'
                    noDataText='Nema pronađenih redaka'
                    pageText='Stranica'
                    ofText='od'
                    rowsText='redaka'
                />

            </div>
        );
    }
}

export default VrsteNastave;