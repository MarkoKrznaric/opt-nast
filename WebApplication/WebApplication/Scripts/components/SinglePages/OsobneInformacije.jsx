﻿import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class OsobneInformacije extends React.Component {
    constructor() {
        super();
        this.state = {
            ime: "",
            prezime: "",
            mail: "",
            zavod: "",
            radnoMjesto: "",
            normaSati: ""
        };

    }


    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/OsobneInformacijeJSON/GetList/" + this.props.idUsername;

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                    this.setState({
                        ime: json.ime,
                        prezime: json.prezime,
                        mail: json.mail,
                        zavod: json.zavod,
                        radnoMjesto: json.radnoMjesto,
                        normaSati: json.normaSati
                    });
                
            }
        };

        xhr.send();
    }

   

    render() {
        

        return (
            <div className="container">
                <h1>Osobne informacije</h1>

                <br />
                
                <br /><br />
                <p>Ime:   <strong>{this.state.ime}</strong></p>
                <p>Prezime:   <strong>{this.state.prezime}</strong></p>
                <p>Mail:   <strong>{this.state.mail}</strong></p>
                <p>Zavod:   <strong>{this.state.zavod}</strong></p>
                <p>Radno mjesto:   <strong>{this.state.radnoMjesto}</strong></p>
                <p>Norma sati za dodijeljeno radno mjesto:   <strong>{this.state.normaSati}</strong></p>

            </div>
        );
    }
}
export default OsobneInformacije;