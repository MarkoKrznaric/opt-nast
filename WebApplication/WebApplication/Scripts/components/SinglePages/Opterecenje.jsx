﻿import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import OdaberiPredmet from './Opterecenje/OdaberiPredmet';
import OdaberiAkGod from './NastavneAktivnosti/OdaberiAkGod';
import OdaberiSem from './NastavneAktivnosti/OdaberiSem';
import OdaberiOsobu from './Opterecenje/OdaberiOsobu';

import AkGodSelect from './NastavneAktivnosti/AkGodSelect';
import SemestarSelect from './NastavneAktivnosti/SemestarSelect';
import VrstaNastaveSelect from './NastavneAktivnosti/VrstaNastaveSelect';

class Opterecenje extends React.Component {
    constructor() {
        super();
        this.state = {
            prikazi: false,
            submit: false,
            post: false,
            delete: false,
            id: '',
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                idAkGodina: "",
                idSemestar: "",
                idVrNast: "",
                skoSati: "",
                datum: "",
            },
            predmet: "",
            studij: "",
            semestar: "",
            brSatiPred: "",
            brSatiVjez: "",
            brSatiSem: "",
            rows: [],
            odabirOsobe: "",
            odabirPredmeta: "",
            odabirAkGod: "",
            odabirSem: "",
            sveukupno: "",
            sveukSatiPred: "",
            sveukSatiVjez: "",
            sveukSatiSem: ""
        };
        this.statetest = this.statetest.bind(this);
        this.changeOdabir = this.changeOdabir.bind(this);
        this.changeOdabirAkGod = this.changeOdabirAkGod.bind(this);
        this.changeOdabirSem = this.changeOdabirSem.bind(this);
        this.changeOdabirOsobe = this.changeOdabirOsobe.bind(this);
        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleSubmitButton = this.handleSubmitButton.bind(this);
    }



    changeOdabir(newValue) {
        this.setState({
            odabirPredmeta: newValue
        });
    }

    changeOdabirAkGod(newValue) {
        this.setState({
            odabirAkGod: newValue
        });
    }

    changeOdabirSem(newValue) {
        this.setState({
            odabirSem: newValue
        });
    }
    changeOdabirOsobe(newValue) {
        this.setState({
            odabirOsobe: newValue
        });
    }

    componentWillUpdate(nextProps, nextState) {
        if ((nextState.submit == true) || (nextState.post != this.state.post) || (nextState.delete != this.state.delete)) {
            const xhr = new XMLHttpRequest();
            const url = "/OpterecenjeRasporedenoJSON/GetList/?odabirPredmeta=" + nextState.odabirPredmeta + "&odabirOsobe=" + nextState.odabirOsobe + "&odabirAkGod=" + nextState.odabirAkGod + "&odabirSem=" + nextState.odabirSem;

            xhr.open('GET', url, true);
            xhr.responseType = 'json';

            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    const json = xhr.response;
                    if (json.meta.error === 0) {
                        this.setState({
                            sveukupno: json.data.ukupnoSati,
                            sveukSatiPred: json.data.ukupnoSatiPred,
                            sveukSatiVjez: json.data.ukupnoSatiVjez,
                            sveukSatiSem: json.data.ukupnoSatiSem,
                            submit: false,
                            post: false,
                            delete: false,
                            loading: false,
                            rows: json.data.list
                        });
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                }
            };
            xhr.send();
        }
        if (nextState.post == true) {
            const xhr = new XMLHttpRequest();
            const url = "/NastavneAktivnostiJSON/Post";

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-type", "application/json");

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const json = JSON.parse(xhr.responseText);
                    if (json.meta.error === 0) {
                        alert("Uspjesno");
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                    this.setState({
                        post: false
                    });
                }
            };

            const data = JSON.stringify({
                "SkoSati": nextState.skoSati,
                "Datum": nextState.datum,
                "IdVrNast": nextState.idVrNast,
                "OdabirPredmeta": nextState.odabirPredmeta,
                "OdabirAkGod": nextState.odabirAkGod,
                "OdabirSem": nextState.odabirSem,
                "IdUsername": this.props.idUsername
            });
            xhr.send(data);
        }
        if (nextState.delete == true) {
            const xhr = new XMLHttpRequest();
            const url = "/NastavneAktivnostiJSON/Delete/" + nextState.id;

            xhr.open("DELETE", url, true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const json = JSON.parse(xhr.responseText);
                    if (json.meta.error === 0) {
                        alert("Uspjesno");
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                    this.setState({
                        delete: false
                    });
                }
            }
            xhr.send();
        }
    }

    requestPost(SkoSati, Datum, IdVrNast = 0) {
        this.setState({
            post: true,
            skoSati: SkoSati,
            datum: Datum,
            idVrNast: IdVrNast
        });
    }

    requestDelete(ids) {
        this.setState({
            delete: true,
            id: ids,
        });
    }

    requestPut(id, skoSati, datum, idAkGodina = 0, idSemestar = 0, idVrNast = 0, idPredmet = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/NastavneAktivnostiJSON/Put/" + id;

        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.setState({
                    });
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "SkoSati": skoSati,
            "Datum": datum,
            "IdAkGodina": idAkGodina,
            "IdSemestar": idSemestar,
            "IdVrNast": idVrNast
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }

        const emptyData = {
            skoSati: null,
            datum: null,
            idVrNast: null
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            skoSati: "",
            datum: "",
            vrNast: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    handleSubmitButton() {
        this.setState({
            submit: true,
            prikazi: true
        });
    }

    render() {
        const columns = [{
            Header: 'Predmet',
            accessor: 'predmet',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.skoSati = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Studij',
            accessor: 'studij',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.datum = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Semestar',
            accessor: 'semestar',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.datum = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
            }, {
                Header: 'SatiPred',
                accessor: 'satiPred',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'SatiVjez',
                accessor: 'satiVjez',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
            Header: 'SatiSem',
            accessor: 'satiSem',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.datum = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Alati',
            show: !this.props.disableTools,
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.skoSati,
                                            this.state.editingData.datum,
                                            this.state.editingData.idVrNast,
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.skoSati,
                                            this.state.editingData.datum,
                                            this.state.editingData.idVrNast,
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;
                                        data.sifra = row.original.sifra;
                                        data.naziv = row.original.naziv;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];

        
        return (

            <div className="ss" align="center">
                {this.state.prikazi == true ? (
                    <div className="tablicaOpterecenje">
                        <br />
                        <br />
                        <br />
                        <br />
                        <div className="listaOdabira">
                        <p><strong>Osoba:</strong></p>
                        <div><OdaberiOsobu idUsername={this.props.idUsername} changeOdabirOsobe={this.changeOdabirOsobe} /></div>
                        <p><strong>Predmet:</strong></p>
                        <div><OdaberiPredmet idUsername={this.props.idUsername} changeOdabir={this.changeOdabir} /></div>
                        <p><strong>Akademska godina:</strong></p>
                        <div><OdaberiAkGod idUsername={this.props.idUsername} changeOdabirAkGod={this.changeOdabirAkGod} /></div>
                        <p><strong>Semestar:</strong></p>
                        <div><OdaberiSem idUsername={this.props.idUsername} changeOdabirSem={this.changeOdabirSem} /></div>
                        <br />
                        <a onClick={this.handleSubmitButton} className="btn btn-primary">Traži</a>
                        </div>

                        <div className="tablica">

                        <h3>Podaci o opterećenju</h3>

                        <br />
                        {!this.props.disableTools && (
                            <a onClick={this.handleAdd} className="btn btn-success">Dodaj aktivnost <em className="fa fa-plus"></em></a>
                            )}
                        <br />
                        
                        <ReactTable
                            data={this.state.rows}
                            columns={columns}
                            filterable={true}
                            loading={this.state.loading}
                            defaultPageSize={5}
                            className="-striped -highlight"
                            previousText='Prethodna'
                            nextText='Sljedeća'
                            loadingText='Učitavanje...'
                            noDataText='Nema pronađenih redaka'
                            pageText='Stranica'
                            ofText='od'
                            rowsText='redaka'
                            />
                        </div>
                        <div className="tablicaDesno">
                            <h3>Ukupno norma sati</h3>
                            <table>
                                <tr>
                                    <th>Predavanja</th>
                                    <td>{this.state.sveukSatiPred}</td>
                                </tr>
                                <tr>
                                    <th>Vježbe</th>
                                    <td>{this.state.sveukSatiVjez}</td>
                                </tr>
                                <tr>
                                    <th>Seminari</th>
                                    <td>{this.state.sveukSatiSem}</td>
                                </tr>
                                <tr>
                                    <th>UkupnoNormaSati</th>
                                    <td>{this.state.sveukupno}</td>
                                </tr>
                            </table>
                        </div>


                    </div>
                ) : (
                        <div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <div className="listaOdabira">
                            <p><strong>Osoba:</strong></p>
                            <div><OdaberiOsobu idUsername={this.props.idUsername} changeOdabirOsobe={this.changeOdabirOsobe} /></div>
                            <p><strong>Predmet:</strong></p>
                            <div><OdaberiPredmet idUsername={this.props.idUsername} changeOdabir={this.changeOdabir} /></div>
                            <p><strong>Akademska godina:</strong></p>
                            <div><OdaberiAkGod idUsername={this.props.idUsername} changeOdabirAkGod={this.changeOdabirAkGod} /></div>
                            <p><strong>Semestar:</strong></p>
                            <div><OdaberiSem idUsername={this.props.idUsername} changeOdabirSem={this.changeOdabirSem} /></div>
                            <br />
                                <a onClick={this.handleSubmitButton} className="btn btn-primary">Traži</a>
                            </div>
                        </div>
                    )}
            </div>
        );
    }
}
export default Opterecenje;