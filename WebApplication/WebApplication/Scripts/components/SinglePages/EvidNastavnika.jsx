﻿import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'
import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import VrstaNastaveSelect from './NastavneAktivnosti/VrstaNastaveSelect';

class EvidNastavnika extends React.Component {
    constructor() {
        super();
        this.state = {
            id: '',
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                idVrNast: "",
                skoSati: "",
                datum: "",
            },
            idVrNast: "",
            skoSati: "",
            datum: "",
            akGodina: "",
            semestar: "",
            rows: [],
            raspPred: "",
            raspVjez: "",
            raspSem: "",
            ukPred: "",
            ukVjez: "",
            ukSem: ""
        };
        this.statetest = this.statetest.bind(this);
        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

    componentDidMount() {
        this.requestList();
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/EvidNastavnikaJSON/GetList/?odabirAkGod=" + this.props.odabirAkGod + "&ime=" + this.props.ime + "&prezime=" + this.props.prezime;

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({
                        loading: false,
                        rows: json.data.list,
                        raspPred: json.data.ukupnoSatiPredRasp,
                        raspVjez: json.data.ukupnoSatiVjezRasp,
                        raspSem: json.data.ukupnoSatiSemRasp,
                        ukPred: json.data.ukupnoSatiPred,
                        ukVjez: json.data.ukupnoSatiVjez,
                        ukSem: json.data.ukupnoSatiSem
                    });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };
        xhr.send();
    }



    requestPost(skoSati, datum, idVrNast = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/EvidPredJSON/Post";

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "SkoSati": skoSati,
            "Datum": datum,
            "IdVrNast": idVrNast,
            "OdabirPredmeta": this.props.idPred,
            "OdabirAkGod": this.state.akGodina,
            "OdabirSem": this.state.semestar,
            "IdUsername": this.props.idUsername
        });
        xhr.send(data);
    }

    requestDelete(ids) {
        const xhr = new XMLHttpRequest();
        const url = "/NastavneAktivnostiJSON/Delete/" + ids;

        xhr.open("DELETE", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        }
        xhr.send();
    }

    requestPut(id, skoSati, datum, idAkGodina = 0, idSemestar = 0, idVrNast = 0, idPredmet = 0) {
        const xhr = new XMLHttpRequest();
        const url = "/NastavneAktivnostiJSON/Put/" + id;

        xhr.open("PUT", url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.setState({
                    });
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        }

        const data = JSON.stringify({
            "SkoSati": skoSati,
            "Datum": datum,
            "IdAkGodina": idAkGodina,
            "IdSemestar": idSemestar,
            "IdVrNast": idVrNast
        });
        xhr.send(data);
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }

        const emptyData = {
            skoSati: null,
            datum: null,
            idVrNast: null
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            skoSati: "",
            datum: "",
            vrNast: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {
        const divStyle = {
            color: 'red',
        };
        const divStyleGreen = {
            color: 'green',
        };
        const columns = [{
            Header: 'Predmet',
            accessor: 'predmet',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.skoSati = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
                Header: 'Studij',
                accessor: 'studij',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'Semestar',
                accessor: 'semestar',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'SatiPred',
                accessor: 'satiPred',
                Cell: row => (
                    <div>
                        {parseInt(row.value) < parseInt(row.original.raspPred) ? (
                            <div style={divStyle}>{row.value}</div>
                        ) : (
                                <div style={divStyleGreen}>{row.value}</div>
                            )}
                    </div>
                )
            }, {
                Header: 'SatiVjez',
                accessor: 'satiVjez',
                Cell: row => (
                    <div>
                        {parseInt(row.value) < parseInt(row.original.raspVjez) ? (
                            <div style={divStyle}>{row.value}</div>
                        ) : (
                                <div style={divStyleGreen}>{row.value}</div>
                            )}
                    </div>
                )
            }, {
                Header: 'SatiSem',
                accessor: 'satiSem',
                Cell: row => (
                    <div>
                        {parseInt(row.value) < parseInt(row.original.raspSem) ? (
                            <div style={divStyle}>{row.value}</div>
                        ) : (
                                <div style={divStyleGreen}>{row.value}</div>
                            )}
                    </div>
                )
            }, {
                Header: 'RaspPred',
                accessor: 'raspPred',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'RaspVjez',
                accessor: 'raspVjez',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
                Header: 'RaspSem',
                accessor: 'raspSem',
                Cell: row => (
                    <div>
                        {this.state.editingRow === row.index ? (
                            <div
                                className="form-control"
                                contentEditable
                                suppressContentEditableWarning
                                onBlur={e => {
                                    const data = this.state.editingData;
                                    data.datum = e.target.innerHTML;
                                    this.setState({
                                        editingData: data
                                    });
                                }}
                            >{row.value}</div>
                        ) : (
                                <p>{row.value}</p>
                            )}
                    </div>
                )
            }, {
            Header: 'Alati',
            show: !this.props.disableTools,
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.skoSati,
                                            this.state.editingData.datum,
                                            this.state.editingData.idVrNast,
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.skoSati,
                                            this.state.editingData.datum,
                                            this.state.editingData.idVrNast,
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;
                                        data.sifra = row.original.skoSati;
                                        data.naziv = row.original.datum;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];


        return (

            <div align="center">
                <div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <div align="center" className="malaTablica">
                        <h4>Ukupno odrađeno/raspoređeno norma sati nastavnika</h4>
                    <table>
                        <tr>
                            <th>OdradPred</th>
                            <th>OdradVjez</th>
                            <th>OdradSem</th>
                            <th>RaspPred</th>
                            <th>RaspVjez</th>
                            <th>RaspSem</th>
                        </tr>
                        <tr>
                            {this.state.ukPred < this.state.raspPred ? (
                                <td style={divStyle}>{this.state.ukPred}</td>
                            ) : (
                                    <td style={divStyleGreen}>{this.state.ukPred}</td>
                                )}
                            {this.state.ukVjez < this.state.raspVjez ? (
                                <td style={divStyle}>{this.state.ukVjez}</td>
                            ) : (
                                    <td style={divStyleGreen}>{this.state.ukVjez}</td>
                                )}
                            {this.state.ukSem < this.state.raspSem ? (
                                <td style={divStyle}>{this.state.ukSem}</td>
                            ) : (
                                    <td style={divStyleGreen}>{this.state.ukSem}</td>
                                )}

                            <td>{this.state.raspPred}</td>
                            <td>{this.state.raspVjez}</td>
                            <td>{this.state.raspSem}</td>
                        </tr>
                    </table>
                    </div>

                    <h3>Odrađeno/raspodjeljeno norma sati po predmetu</h3>

                    <br />
                    {!this.props.disableTools && (
                        <a onClick={this.handleAdd} className="btn btn-success">Dodaj aktivnost <em className="fa fa-plus"></em></a>
                    )}

                    <br /><br />
                    <ReactTable
                        data={this.state.rows}
                        columns={columns}
                        filterable={true}
                        loading={this.state.loading}
                        defaultPageSize={5}
                        className="-striped -highlight"
                        previousText='Prethodna'
                        nextText='Sljedeća'
                        loadingText='Učitavanje...'
                        noDataText='Nema pronađenih redaka'
                        pageText='Stranica'
                        ofText='od'
                        rowsText='redaka'
                    />
                </div>
            </div>
        );
    }
}
export default EvidNastavnika;