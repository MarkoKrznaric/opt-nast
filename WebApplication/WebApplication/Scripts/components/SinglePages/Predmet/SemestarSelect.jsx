﻿import React from 'react';
import Select from 'react-select';

class SemestarSelect extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedOption: '',
            list: []
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.requestList();
    }

    handleChange(selectedOption) {
        this.setState({ selectedOption });
        this.props.changeEditingRow(null, "semestar", selectedOption.value);
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/Select/Semestar";

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {
                    this.setState({ list: json.data.list });
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        xhr.send();
    }

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        return (
            <Select
                name="form-field-name"
                value={value}
                onChange={this.handleChange}
                clearable={false}
                options={this.state.list}
            />
        );
    }
}

export default SemestarSelect;