﻿import React from 'react';
import ReactTable from 'react-table';

import 'react-table/react-table.css';

import PredmetSelect from './OpterecenjePredmeta/PredmetSelect';
import VrstaNastaveSelect from './OpterecenjePredmeta/VrstaNastaveSelect';
import OdaberiPredmetOpt from './OpterecenjePredmeta/OdaberiPredmetOpt';

class OpterecenjaPredmeta extends React.Component {
    constructor() {
        super();
        this.state = {
            id: '',
            post: false,
            delete: false,
            put: false,
            loading: true,
            isAdding: false,
            editingRow: -1,
            editingData: {
                satiUk: null,
                idVrstaNastave: null
            },
            rows: [],
            satiUk: "",
            idVrstaNastave: "",
            odabirPredmeta: ""
        };
        this.statetest = this.statetest.bind(this);
        this.changeOdabir = this.changeOdabir.bind(this);

        this.changeEditingRow = this.changeEditingRow.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

    changeOdabir(newValue) {
        this.setState({
            odabirPredmeta: newValue
        });
    }

    componentWillUpdate(nextProps, nextState) {
        if ((nextState.odabirPredmeta != this.state.odabirPredmeta) || (nextState.put != this.state.put) || (nextState.post != this.state.post) || (nextState.delete != this.state.delete)) {
            
            const xhr = new XMLHttpRequest();
            const url = "/OpterecenjePredmetaJSON/GetList/?odabirPredmeta=" + nextState.odabirPredmeta;

            xhr.open('GET', url, true);
            xhr.responseType = 'json';

            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    const json = xhr.response;
                    if (json.meta.error === 0) {
                        this.setState({
                            put: false,
                            post: false,
                            delete: false,
                            loading: false,
                            rows: json.data.list
                        });
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                }
            };

            xhr.send();
        }
        if (nextState.post == true) {

            const xhr = new XMLHttpRequest();
            const url = "/OpterecenjePredmetaJSON/Post";

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-type", "application/json");

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const json = JSON.parse(xhr.responseText);
                    if (json.meta.error === 0) {
                        alert("Uspjesno");
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                    this.setState({
                        post: false
                    });
                }
            };

            const data = JSON.stringify({
                "SatiUk": nextState.satiUk,
                "IdVrstaNastave": nextState.idVrstaNastave,
                "OdabirPredmeta": nextState.odabirPredmeta,
            });
            xhr.send(data);
        }
        if (nextState.delete == true) {

            const xhr = new XMLHttpRequest();
            const url = "/OpterecenjePredmetaJSON/Delete/" + nextState.id;

            xhr.open("DELETE", url, true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const json = JSON.parse(xhr.responseText);
                    if (json.meta.error === 0) {
                        alert("Uspjesno");
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                    this.setState({
                        delete: false
                    });
                }
            }
            xhr.send();
        }
        if (nextState.put == true) {
            const xhr = new XMLHttpRequest();
            const url = "/OpterecenjePredmetaJSON/Put/" + nextState.id;

            xhr.open("PUT", url, true);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const json = JSON.parse(xhr.responseText);
                    if (json.meta.error === 0) {
                        this.setState({
                            put: false
                        });
                        alert("Uspjesno");
                    } else {
                        alert("Error " + json.meta.error + "\n" + json.meta.message);
                    }
                    
                }
            }

            const data = JSON.stringify({
                "SatiUk": nextState.satiUk,
                "IdVrstaNastave": nextState.idVrstaNastave,
                "OdabirPredmeta": nextState.odabirPredmeta,
            });
            xhr.send(data);
        }
    }


    requestPost(SatiUk, IdVrstaNastave = 0) {
        this.setState({
            post: true,
            satiUk: SatiUk,
            idVrstaNastave: IdVrstaNastave,
        });
    }

    requestDelete(ids) {
        this.setState({
            delete: true,
            id: ids,
        });
    }

    requestPut(ids, SatiUk, IdVrstaNastave = 0) {
        this.setState({
            put: true,
            id: ids,
            satiUk: SatiUk,
            idVrstaNastave: IdVrstaNastave,
        });
        
    }

    changeEditingRow(e, accessor, newValue) {
        const data = this.state.editingData;
        data[accessor] = newValue;
        this.setState({
            editingData: data
        });
    }

    statetest() {
        console.log(this.state);
    }

    addItem() {
        if (this.state.isAdding === true) {
            return;
        }
        const emptyData = {
            satiUk: null,
            idVrstaNastave: null
        }

        const addedRow = this.state.rows;
        addedRow.unshift({
            satiUk: "",
            vrstaNastave: ""
        });

        this.setState({
            isAdding: true,
            editingRow: 0,
            editingData: emptyData,
            rows: addedRow
        });
    }

    undoItem() {
        const undoRow = this.state.rows;
        undoRow.shift();
        this.setState({
            isAdding: false,
            rows: undoRow
        });
    }

    handleAdd() {
        this.addItem();
    }

    render() {
        const columns = [{
            Header: 'SatiUk',
            accessor: 'satiUk',
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <div
                            className="form-control"
                            contentEditable
                            suppressContentEditableWarning
                            onBlur={e => {
                                const data = this.state.editingData;
                                data.satiUk = e.target.innerHTML;
                                this.setState({
                                    editingData: data
                                });
                            }}
                        >{row.value}</div>
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'VrstaNastave',
            accessor: 'vrstaNastave',
            style: { overflow: 'initial' },
            Cell: row => (
                <div>
                    {this.state.editingRow === row.index ? (
                        <VrstaNastaveSelect changeEditingRow={this.changeEditingRow} />
                    ) : (
                            <p>{row.value}</p>
                        )}
                </div>
            )
        }, {
            Header: 'Alati',
            show: !this.props.disableTools,
            Cell: row => {
                return (
                    <div align="center">
                        {this.state.editingRow === row.index ? (
                            <div>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.requestPost(
                                            this.state.editingData.satiUk,
                                            this.state.editingData.idVrstaNastave
                                        );
                                    } else {
                                        this.requestPut(
                                            row.original.id,
                                            this.state.editingData.satiUk,
                                            this.state.editingData.idVrstaNastave
                                        );
                                    }
                                    this.setState({ isAdding: false, editingRow: -1 });
                                }} className="btn btn-default"><em className="fa fa-check"></em></a>
                                <a onClick={() => {
                                    if (this.state.isAdding) {
                                        this.undoItem();
                                    }
                                    this.setState({ editingRow: -1 });
                                }} className="btn btn-danger"><em className="fa fa-times" ></em></a>
                            </div>
                        ) : (
                                <div>
                                    <a onClick={() => {
                                        const data = this.state.editingData;
                                        data.sifra = row.original.satiUk;

                                        let rowIndex = row.index;
                                        if (this.state.isAdding) {
                                            this.undoItem();
                                            rowIndex--;
                                        }
                                        this.setState({
                                            editingRow: rowIndex,
                                            editingData: data
                                        });
                                    }} className="btn btn-default"><em className="fa fa-pencil" ></em></a>
                                    <a onClick={() => {
                                        this.requestDelete(row.original.id);
                                        this.setState({
                                            editingRow: -1
                                        });
                                    }} className="btn btn-danger"><em className="fa fa-trash" ></em></a>
                                </div>
                            )}
                    </div>
                );
            }
        }];

        return (
            <div align="center">
                {this.state.odabirPredmeta != "" ? (
                    <div>
                        <br /><br />
                        <div className="odabirMojZavodd">
                        <h3>Odaberite predmet</h3>
                        <div><OdaberiPredmetOpt changeOdabir={this.changeOdabir} /></div>
                        </div>
                        
                        <h1>Opterećenja predmeta</h1>
                        {!this.props.disableTools && (
                            <a onClick={this.handleAdd} className="btn btn-success">Dodaj</a>
                        )}
                        <ReactTable
                            data={this.state.rows}
                            columns={columns}
                            filterable={true}
                            loading={this.state.loading}
                            defaultPageSize={5}
                            className="-striped -highlight"
                            previousText='Prethodna'
                            nextText='Sljedeća'
                            loadingText='Učitavanje...'
                            noDataText='Nema pronađenih redaka'
                            pageText='Stranica'
                            ofText='od'
                            rowsText='redaka'
                        />
                    </div>

                ) : (
                        <div className="odabirMojZavodd">
                            <br /> <br />
                            <h3>Odaberite predmet</h3>
                            <div><OdaberiPredmetOpt changeOdabir={this.changeOdabir} /></div>
                        </div>
                    )}
            </div>
        );
    }
}

export default OpterecenjaPredmeta;