﻿import React from 'react';
import Select from 'react-select';

class UlogaSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: {
                value: this.props.uloga,
                label: this.props.uloga
            }
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(selectedOption) {
        this.setState({ selectedOption });
        this.props.changeEditingRow(null, "uloga", selectedOption.value);
    }

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        return (
            <Select
                name="form-field-name"
                value={value}
                onChange={this.handleChange}
                clearable={false}
                options={[{
                    value: "Voditelj zavoda",
                    label: "Voditelj zavoda"
                }, {
                    value: "Nastavnik",
                    label: "Nastavnik"
                }]}
            />
        );
    }
}

export default UlogaSelect;