﻿import React from 'react';

class LoginForm extends React.Component {
    constructor() {
        super();
        this.handleLoginClick = this.handleLoginClick.bind(this);
    }

    handleLoginClick(e) {
        e.preventDefault();
        const username = this.refs.usrInput.value;
        const password = this.refs.passInput.value;

        const xhr = new XMLHttpRequest();
        const url = "/Account/Authentification/?username=" + username + "&password=" + password;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    this.props.changeLogState(username, json.data.role);
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };
        const data = JSON.stringify({});
        xhr.send(data);
    }

    

    render() {
        return (
            <div className="form-box">
                <div className="form-top">
                    <span className="glyphicon glyphicon-list-alt"></span>
                    <h3>Prijavite se</h3>
                    <p>Unesite korisničko ime i lozinku:</p>
                </div>
                <div className="form-bottom">
                    <form role="form" action="" method="post" className="login-form">
                        <div className="form-group">
                            <i className="glyphicon glyphicon-user"></i>
                            <input className="form-control" placeholder="Korisničko ime" ref='usrInput' />
                        </div>
                        <div className="form-group">
                            <i className="glyphicon glyphicon-lock"></i>
                            <input type="password" className="form-control" placeholder="Lozinka" ref='passInput' />
                        </div>
                    </form>
                </div>
                <button className="btn btn-primary" onClick={this.handleLoginClick}>Prijavite se</button>
            </div>

        );
    }
}

export default LoginForm;