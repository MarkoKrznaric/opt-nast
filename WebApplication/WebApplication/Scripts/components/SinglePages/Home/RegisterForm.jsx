﻿import React from 'react';

class RegisterForm extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedRole: 'Nastavnik'
        }
        this.handleRegisterClick = this.handleRegisterClick.bind(this);
        this.handleRoleChange = this.handleRoleChange.bind(this);
        this.handleLoginClick = this.handleLoginClick.bind(this);
    }

    requestList() {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/GetList";

        xhr.open('GET', url, true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                const json = xhr.response;
                if (json.meta.error === 0) {

                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };
        xhr.send();
    }

    handleLoginClick(e) {
        const xhr = new XMLHttpRequest();
        const url = "/OsobaJSON/Post";


        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
                this.requestList();
            }
        };

        const data = JSON.stringify({
            "Username": this.refs.usrInput.value,
            "Password": this.refs.passInput.value,
            "Ime": this.refs.imeInput.value,
            "Prezime": this.refs.prezInput.value,
            "Uloga": this.state.selectedRole,
            "Mail": this.refs.mailInput.value,
        });
        xhr.send(data);
    }

    requestRegister(username, password, ime, prezime, role, mail) {
        const xhr = new XMLHttpRequest();
        const url = "/Account/Register";

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const json = JSON.parse(xhr.responseText);
                if (json.meta.error === 0) {
                    alert("Uspjesno");
                } else {
                    alert("Error " + json.meta.error + "\n" + json.meta.message);
                }
            }
        };

        const data = JSON.stringify({
            "Username": username,
            "Password": password,
            "Ime": ime,
            "Prezime": prezime,
            "Role": role,
            "Mail": mail
        });
        xhr.send(data);
    }

    handleRegisterClick(e) {
        e.preventDefault();
        this.requestRegister(
            this.refs.usrInput.value,
            this.refs.passInput.value,
            this.refs.imeInput.value,
            this.refs.prezInput.value,
            this.state.selectedRole,
            this.refs.mailInput.value);
    }

    handleRoleChange() {
        this.setState({ selectedRole: this.refs.ulogInput.value });
    }

    render() {
        return (
            <div className="form-box">
                <div className="form-top">
                    <div className="row">
                        <div className="col-sm-9">
                            <h3>Registrirajte se</h3>
                            <p>Ispunite obrazac kako biste dobili pristup</p>
                        </div>
                        <div className="col-sm-3">
                            <i className="fa fa-pencil fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div className="form-bottom">
                    <form role="form" action="" method="post" className="registration-form">
                        
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <span className="input-group-addon"><i className="fa fa-user-circle fa-fw"></i></span>
                                    <input type="text" className="form-control" placeholder="Unesite ime" ref="imeInput" />
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="form-group">
                                    <span className="input-group-addon"><i className="fa fa-user-circle fa-fw"></i></span>
                                    <input type="text" className="form-control" placeholder="Unesite prezime" ref="prezInput" />
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <span className="input-group-addon"><i className="glyphicon glyphicon-envelope"></i></span>
                            <input type="text" className="form-control" placeholder="Unesi e-mail" ref="mailInput" />
                        </div>
                        <div className="form-group">
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                            <input type="text" className="form-control" placeholder="Unesite korisničko ime" ref="usrInput" />
                        </div>
                        <div className="form-group">
                            <span className="input-group-addon"><i className="fa fa-key fa-fw"></i></span>
                            <input type="text" className="form-control" placeholder="Unesite lozinku" ref="passInput" />
                        </div>
                        <div className="form-group">
                            <span className="input-group-addon"><i className="fa fa-id-badge fa-fw"></i></span>
                            <select onChange={this.handleRoleChange} className="form-control" ref="ulogInput">
                                <option value="Nastavnik">Nastavnik</option>
                                <option value="Voditelj zavoda">Voditelj zavoda</option>
                                <option value="Administrator">Administrator</option>
                            </select>
                        </div>
                        <button className="btn btn-primary" onClick={this.handleLoginClick}>Registrirajte se</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default RegisterForm;