﻿import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'

import Home from './SinglePages/Home';
import Header from './Layout/Header';
import Zavodi from './SinglePages/Zavodi';
import Osobe from './SinglePages/Osobe';
import RadnaMjesta from './SinglePages/RadnaMjesta';
import Predmeti from './SinglePages/Predmeti';
import VrsteNastave from './SinglePages/VrsteNastave';
import OpterecenjaPredmeta from './SinglePages/OpterecenjaPredmeta';
import PredAkGodSemestri from './SinglePages/PredAkGodSemestri';
import PredAkGodSemOsobe from './SinglePages/PredAkGodSemOsobe';
import PredAkGodSemGrupe from './SinglePages/PredAkGodSemGrupe';
import PredmetiNastavnika from './SinglePages/PredmetiNastavnika';
import OsobneInformacije from './SinglePages/OsobneInformacije';
import NastavneAktivnosti from './SinglePages/NastavneAktivnosti';
import PredmetiZavoda from './SinglePages/PredmetiZavoda';
import NastavniciZavoda from './SinglePages/NastavniciZavoda'; 
import Opterecenje from './SinglePages/Opterecenje';
import MojZavod from './SinglePages/MojZavod';
import EvidPred from './SinglePages/EvidPred';
import EvidNastavnika from './SinglePages/EvidNastavnika';
import EvidPredmeta from './SinglePages/EvidPredmeta';



class App extends React.Component {
    constructor() {
        super();
        this.state = {
            username: 'Guest',
            role: '',
            evidPred: [],
            idPred: null,
            akGodina: "",
            semestar: "",
            odabirAkGod: "",
            ime: "",
            prezime: "",
            naziv: "",
        };
        this.changeLogState = this.changeLogState.bind(this);
        this.handleClickApp = this.handleClickApp.bind(this);
        this.changePredmetState = this.changePredmetState.bind(this);
        this.changeNastavnikState = this.changeNastavnikState.bind(this);
        this.changePredmetiState = this.changePredmetiState.bind(this);
    }

    componentWillMount() {
        if (sessionStorage.getItem("loginSession")) {
            this.setState(JSON.parse(sessionStorage.getItem("loginSession")));
        }
    }

    changeLogState(newUsername, newRole) {
        this.setState({
            username: newUsername,
            role: newRole
        });
        sessionStorage.setItem('loginSession', JSON.stringify(this.state));
    }

    handleClickApp(e) {
        console.log(this.state);
    }

    changePredmetState(idPred, evidPred, akGodina, semestar) {
        this.setState({
            idPred,
            evidPred,
            akGodina,
            semestar
        });
    }

    changeNastavnikState(odabirAkGod, ime, prezime) {
        this.setState({
            odabirAkGod,
            ime,
            prezime
        });
    }

    changePredmetiState(odabirAkGod, naziv, semestar) {
        this.setState({
            odabirAkGod,
            naziv,
            semestar
        });
    }

    render() {
        const propHome = () => {
            return (
                <Home {...this.state} changeLogState={this.changeLogState} />
            );
        };
        return (
            <Router>
                <div>
                    <Header {...this.state} changeLogState={this.changeLogState} />
                    <div className="pages">
                        <Switch>
                            <Route exact path='/' component={propHome} />
                            {this.state.role === 'Nastavnik' && (
                                <Switch>
                                    <Route path='/OsobneInformacije' render={(props) => <OsobneInformacije {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Route path='/PredmetiNastavnika' render={(props) => <PredmetiNastavnika {...props} disableTools={true} idUsername={this.state.username} changePredmetState={this.changePredmetState} />} />
                                    <Route path='/NastavneAktivnosti' render={(props) => <NastavneAktivnosti {...props} idUsername={this.state.username} />} />
                                    <Route path='/Opterecenje' render={(props) => <Opterecenje {...props} disableTools={true} idUsername={this.state.username} />} />
                                    {this.state.idPred !== null && (
                                        <Route path='/EvidPred' render={(props) => <EvidPred {...props} idUsername={this.state.username} akGodina={this.state.akGodina} semestar={this.state.semestar} idPred={this.state.idPred} evidPred={this.state.evidPred} />} />
                                    )}
                                    <Redirect to='/' />
                                </Switch>
                            )}

                            {this.state.role === 'Voditelj zavoda' && (
                                <Switch>
                                    <Route path='/OsobneInformacije' render={(props) => <OsobneInformacije {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Route path='/PredmetiNastavnika' render={(props) => <PredmetiNastavnika {...props} disableTools={true} idUsername={this.state.username} changePredmetState={this.changePredmetState} />} />
                                    <Route path='/PredmetiZavoda' render={(props) => <PredmetiZavoda {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Route path='/NastavneAktivnosti' render={(props) => <NastavneAktivnosti {...props} idUsername={this.state.username} />} />
                                    <Route path='/NastavniciZavoda' render={(props) => <NastavniciZavoda {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Route path='/Opterecenje' render={(props) => <Opterecenje {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Route path='/MojZavod' render={(props) => <MojZavod {...props} disableTools={true} idUsername={this.state.username} changeNastavnikState={this.changeNastavnikState} changePredmetiState={this.changePredmetiState} />} />
                                    {this.state.idPred !== null && (
                                        <Route path='/EvidPred' render={(props) => <EvidPred {...props} idUsername={this.state.username} akGodina={this.state.akGodina} semestar={this.state.semestar} idPred={this.state.idPred} evidPred={this.state.evidPred} />} />
                                    )}
                                    {this.state.ime !== null && (
                                        <Route path='/EvidNastavnika' render={(props) => <EvidNastavnika {...props} disableTools={true} odabirAkGod={this.state.odabirAkGod} ime={this.state.ime} prezime={this.state.prezime} />} />
                                    )}
                                    {this.state.naziv !== null && (
                                        <Route path='/EvidPredmeta' render={(props) => <EvidPredmeta {...props} disableTools={true} odabirAkGod={this.state.odabirAkGod} naziv={this.state.naziv} semestar={this.state.semestar} />} />
                                    )}
                                    <Redirect to='/' />
                                </Switch>
                            )}

                            {this.state.role === 'Administrator' && (
                                <Switch>
                                    <Route path='/Zavodi' component={Zavodi} />
                                    <Route path='/Osobe' component={Osobe} />
                                    <Route path='/RadnaMjesta' component={RadnaMjesta} />
                                    <Route path='/Predmeti' component={Predmeti} />
                                    <Route path='/VrsteNastave' component={VrsteNastave} />
                                    <Route path='/OpterecenjaPredmeta' component={OpterecenjaPredmeta} />
                                    <Route path='/PredAkGodSemestri' component={PredAkGodSemestri} />
                                    <Route path='/PredAkGodSemOsobe' component={PredAkGodSemOsobe} />
                                    <Route path='/PredAkGodSemGrupe' component={PredAkGodSemGrupe} />
                                    <Route path='/Opterecenje' render={(props) => <Opterecenje {...props} disableTools={true} idUsername={this.state.username} />} />
                                    <Redirect to='/' />
                                </Switch>
                            )}

                            <Redirect to='/' />
                        </Switch>
                    </div>
                </div>

            </Router>

        );
    }
}

export default App;
